package ru.joyfolk.functional;

public class Bind {

  public static <R, T> AbstractFunction0<R> bind1st(final Function1<R, T> fun, final T val) {
    return new AbstractFunction0<R>() {
      @Override
      public R apply() {
        return fun.apply(val);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 1=>" + val + ")";
      }
    };
  }


  public static <R, T1, T2> AbstractFunction1<R, T2> bind1st(final Function2<R, T1, T2> fun, final T1 val) {
    return new AbstractFunction1<R, T2>() {
      @Override
      public R apply(T2 arg) {
        return fun.apply(val, arg);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 1=>" + val + ")";
      }
    };
  }

  public static <R, T1, T2> AbstractFunction1<R, T1> bind2nd(final Function2<R, T1, T2> fun, final T2 val) {
    return new AbstractFunction1<R, T1>() {
      @Override
      public R apply(T1 arg) {
        return fun.apply(arg, val);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 2=>" + val + ")";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction2<R, T2, T3> bind1st(final Function3<R, T1, T2, T3> fun, final T1 val) {
    return new AbstractFunction2<R, T2, T3>() {
      @Override
      public R apply(T2 arg0, T3 arg1) {
        return fun.apply(val, arg0, arg1);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 1=>" + val + ")";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction2<R, T1, T3> bind2nd(final Function3<R, T1, T2, T3> fun, final T2 val) {
    return new AbstractFunction2<R, T1, T3>() {
      @Override
      public R apply(T1 arg0, T3 arg1) {
        return fun.apply(arg0, val, arg1);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 2=>" + val + ")";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction2<R, T1, T2> bind3rd(final Function3<R, T1, T2, T3> fun, final T3 val) {
    return new AbstractFunction2<R, T1, T2>() {
      @Override
      public R apply(T1 arg0, T2 arg1) {
        return fun.apply(arg0, arg1, val);
      }

      @Override
      public String toString() {
        return "Bind(" + fun + ", 3=>" + val + ")";
      }
    };
  }

  public static <R, T> AbstractFunction1<R, T> bindUp1st(final Function0<R> fun) {
    return new AbstractFunction1<R, T>() {
      @Override
      public R apply(T arg) {
        return fun.apply();
      }

      @Override
      public String toString() {
        return "BindUp(" + fun + ")";
      }
    };
  }

  public static <R, T1, T2> AbstractFunction2<R, T1, T2> bindUp1st(final Function1<R, T1> fun) {
    return new AbstractFunction2<R, T1, T2>() {
      @Override
      public R apply(T1 arg1, T2 arg2) {
        return fun.apply(arg1);
      }

      @Override
      public String toString() {
        return "BindUp(" + fun + ", _)";
      }
    };
  }

  public static <R, T1, T2> AbstractFunction2<R, T1, T2> bindUp2nd(final Function1<R, T2> fun) {
    return new AbstractFunction2<R, T1, T2>() {
      @Override
      public R apply(T1 arg1, T2 arg2) {
        return fun.apply(arg2);
      }

      @Override
      public String toString() {
        return "BindUp(_, " + fun + ")";
      }
    };
  }


  public static <R, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bindUp1st(final Function2<R, T2, T3> fun) {
    return new AbstractFunction3<R, T1, T2, T3>() {
      @Override
      public R apply(T1 arg1, T2 arg2, T3 arg3) {
        return fun.apply(arg2, arg3);
      }

      @Override
      public String toString() {
        return "BindUp(" + fun + ", _, _)";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bindUp2nd(final Function2<R, T1, T3> fun) {
    return new AbstractFunction3<R, T1, T2, T3>() {
      @Override
      public R apply(T1 arg1, T2 arg2, T3 arg3) {
        return fun.apply(arg1, arg3);
      }

      @Override
      public String toString() {
        return "BindUp(_, " + fun + ", _)";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bindUp3rd(final Function2<R, T1, T2> fun) {
    return new AbstractFunction3<R, T1, T2, T3>() {
      @Override
      public R apply(T1 arg1, T2 arg2, T3 arg3) {
        return fun.apply(arg1, arg2);
      }

      @Override
      public String toString() {
        return "BindUp(_, _, " + fun + ")";
      }
    };
  }


	public static <R, R0, T1, T2> AbstractFunction2<R, T1, T2> bind1st(final Function1<? extends R0, ? super T1> fun1, final Function2<? extends R, ? super R0, ? super T2> fun2) {
  	return new AbstractFunction2<R, T1, T2>() {
  		@Override
      public R apply(T1 arg0, T2 arg1) {
        return fun2.apply(fun1.apply(arg0), arg1);
      }			  	
  	
  		@Override
    	public String toString() {
      	return "Compose1st(" + fun1.toString() + "=>" + fun2.toString() + ")";
    	}  	
  	};
  }


	public static <R, R0, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bind1st(final Function1<? extends R0, ? super T1> fun1, final Function3<? extends R, ? super R0, ? super T2, ? super T3> fun2) {
  	return new AbstractFunction3<R, T1, T2, T3>() {
  		@Override
      public R apply(T1 arg0, T2 arg1, T3 arg2) {
        return fun2.apply(fun1.apply(arg0), arg1, arg2);
      }
  		
  		@Override
    	public String toString() {
      	return "Compose1st(" + fun1.toString() + "=>" + fun2.toString() + ")";
    	}  	
  	};
  }


	public static <R, R0, T1, T2> AbstractFunction2<R, T1, T2> bind2nd(final Function1<? extends R0, ? super T2> fun1, final Function2<? extends R, ? super T1, ? super R0> fun2) {
  	return new AbstractFunction2<R, T1, T2>() {
  		@Override
      public R apply(T1 arg0, T2 arg1) {
        return fun2.apply(arg0, fun1.apply(arg1));
      }			
  
  		@Override
    	public String toString() {
      	return "Compose2nd(" + fun1.toString() + "=>" + fun2.toString() + ")";
    	}  	
  	};
  	
  }


	public static <R, R0, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bind2nd(final Function1<? extends R0, ? super T2> fun1, final Function3<? extends R, ? super T1, ? super R0, ? super T3> fun2) {
  	return new AbstractFunction3<R, T1, T2, T3>() {
  		@Override
      public R apply(T1 arg0, T2 arg1, T3 arg2) {
        return fun2.apply(arg0, fun1.apply(arg1), arg2);
      }
  		
  		@Override
    	public String toString() {
      	return "Compose2nd(" + fun1.toString() + "=>" + fun2.toString() + ")";
    	}  	
  	};
  }


	public static <R, R0, T1, T2, T3> AbstractFunction3<R, T1, T2, T3> bind3rd(final Function1<? extends R0, ? super T3> fun1, final Function3<? extends R, ? super T1, ? super T2, ? super R0> fun2) {
  	return new AbstractFunction3<R, T1, T2, T3>() {
  		@Override
      public R apply(T1 arg0, T2 arg1, T3 arg2) {
        return fun2.apply(arg0, arg1, fun1.apply(arg2));
      }			
  
  		@Override
    	public String toString() {
      	return "Compose3rd(" + fun1.toString() + "=>" + fun2.toString() + ")";
    	}  	
  	};
  }

}