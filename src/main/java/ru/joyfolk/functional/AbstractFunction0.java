package ru.joyfolk.functional;

public abstract class AbstractFunction0<R> implements Function0<R> {
  public <T> AbstractFunction1<R, T> bindUp1st(final Function0<R> fun) {
    return Bind.bindUp1st(this); 
  }
  
  public <R1> AbstractFunction0<R1> compose(Function1<R1, R> fun) {
  	return Compose.compose(this, fun);
  }
}
