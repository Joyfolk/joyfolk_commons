package ru.joyfolk.functional;

public abstract class AbstractFunction3<R, T1, T2, T3> implements Function3<R, T1, T2, T3> {
	public AbstractFunction2<R, T2, T3> bind1st(T1 val) {
		return Bind.bind1st(this, val);
	}
	
	public AbstractFunction2<R, T1, T3> bind2nd(T2 val) {
		return Bind.bind2nd(this, val);
	}
	
	public AbstractFunction2<R, T1, T2> bind3rd(T3 val) {
		return Bind.bind3rd(this, val);
	}		
	
	public <R1> AbstractFunction3<R1, T1, T2, T3> compose(Function1<R1, R> fun) {
	  return Compose.compose(this, fun);
	}	
	
	public <T4> AbstractFunction3<R, T4, T2, T3> bind1st(Function1<T1, T4> fun) {
	  return Bind.bind1st(fun, this);
	}	

	public <T4> AbstractFunction3<R, T1, T4, T3> bind2nd(Function1<T2, T4> fun) {
	  return Bind.bind2nd(fun, this);
	}	

	public <T4> AbstractFunction3<R, T1, T2, T4> bind3rd(Function1<T3, T4> fun) {
	  return Bind.bind3rd(fun, this);
	}	
}
