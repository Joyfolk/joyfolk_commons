package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class InstanceOf<T> implements Predicate<T> {
  private final Class<T> clss;

  public InstanceOf(Class<T> clss) {
    assert clss != null: "Класс не должен быть null";
    this.clss = clss;
  }

  @Override
  public Boolean apply(Object arg) {
    return clss.isAssignableFrom(arg.getClass());
  }

  @Override
  public String toString() {
    return "InstanceOf(" + clss.toString() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(InstanceOf.class, clss);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof InstanceOf && Equals.isEquals(clss, ((InstanceOf) o).clss);
  }

}
