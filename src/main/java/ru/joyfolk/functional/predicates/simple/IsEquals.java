package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class IsEquals<T> implements Predicate<T> {
  final T value;
  public IsEquals(T value) {
    this.value = value;
  }

  @Override
  public Boolean apply(T arg) {
    return Equals.isEquals(value, arg);
  }

  @Override
  public String toString() {
    return "IsEquals(" + value + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(IsEquals.class, value);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof IsEquals && Equals.isEquals(value, ((IsEquals) o).value);
  }
}
