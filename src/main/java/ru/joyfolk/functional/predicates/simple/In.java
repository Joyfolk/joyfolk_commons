package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

import java.util.Collection;

public class In<T> implements Predicate<T> {
  private final Collection<? extends T> col;

  public In(Collection<? extends T> col) {
    assert col != null: "Коллекция не должна быть null";
    this.col = col;
  }

  @Override
  public Boolean apply(T arg) {
    return col.contains(arg);
  }

  @Override
  public String toString() {
    return "In(" + col.toString() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(In.class, col);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof In && Equals.isEquals(col, ((In) o).col);
  }

}
