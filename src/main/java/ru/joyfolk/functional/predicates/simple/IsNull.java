package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;

public class IsNull<T> implements Predicate<T> {
  @Override
  public Boolean apply(T arg) {
    return arg == null;
  }

  @Override
  public String toString() {
    return "IsNull()"; 
  }
 
}
