package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Comparators;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

import java.util.Comparator;

public class IsLarger<T> implements Predicate<T> {
  private final T value;
  private final Comparator<? super T> comparator;
  private final Comparators.NullStrategy nullStrategy;

  public static <T extends Comparable<? super T>> IsLarger<T> than(T value) {
    return than(value, Comparators.<T>comparable());
  }

  public static <T> IsLarger<T> than(T value, Comparator<? super T> comparator) {
    return than(value, comparator, Comparators.NullStrategy.NULL_LAST);
  }

  public static <T> IsLarger<T> than(T value, Comparator<? super T> comparator, Comparators.NullStrategy nullStrategy) {
    return new IsLarger<T>(value, comparator, nullStrategy);
  }

  private IsLarger(T value, Comparator<? super T> comparator, Comparators.NullStrategy nullStrategy) {
    this.value = value;
    this.comparator = comparator;
    this.nullStrategy = nullStrategy;
  }

  @Override
  public Boolean apply(T arg) {
    return Comparators.compare(value, arg, nullStrategy, comparator) < 0;
  }

  @Override
  public String toString() {
    return "IsLarger.than(" + value + "/" + nullStrategy + ")"; 
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(IsLarger.class, value, nullStrategy);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof IsLarger
        && Equals.isEquals(value, ((IsLarger) o).value)
        && Equals.isEquals(comparator, ((IsLarger) o).comparator)
        && Equals.isEquals(nullStrategy, ((IsLarger) o).nullStrategy);
  }
  
}
