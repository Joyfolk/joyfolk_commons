package ru.joyfolk.functional.predicates.simple;

import ru.joyfolk.functional.predicates.Predicate;

public class NumberPredicates {

  public static Predicate<Integer> isEvenInt() {
    return isEvenInt;
  }

  public static Predicate<Integer> isOddInt() {
    return isOddInt;
  }

  public static Predicate<Long> isEvenLong() {
    return isEvenLong;
  }

  public static Predicate<Long> isOddLong() {
    return isOddLong;
  }

  public static <T extends Comparable<? super T>> Predicate<T> isLarger(T value) {
    return IsLarger.than(value);
  }

  public static <T extends Comparable<? super T>> Predicate<T> isLargerOrEquals(T value) {
    return IsLargerOrEquals.than(value);
  }

  public static <T extends Comparable<? super T>> Predicate<T> isLesser(T value) {
    return IsLesser.than(value);
  }

  public static <T extends Comparable<? super T>> Predicate<T> isLesserOrEquals(T value) {
    return IsLesserOrEquals.than(value);
  }


  private static final Predicate<Integer> isEvenInt = new Predicate<Integer>() {
    @Override
    public Boolean apply(Integer arg) {
      return arg % 2 == 0;
    }

    @Override
    public String toString() {
      return "IsEven(int)";
    }
  };

  private static final Predicate<Integer> isOddInt = new Predicate<Integer>() {
    @Override
    public Boolean apply(Integer arg) {
      return arg % 2 == 1;  
    }

    @Override
    public String toString() {
      return "IsOdd(int)";
    }
  };

  private static final Predicate<Long> isEvenLong = new Predicate<Long>() {
    @Override
    public Boolean apply(Long arg) {
      return arg % 2 == 0;
    }

    @Override
    public String toString() {
      return "IsEven(long)";
    }
  };

  private static final Predicate<Long> isOddLong = new Predicate<Long>() {
    @Override
    public Boolean apply(Long arg) {
      return arg % 2 == 1;
    }

    @Override
    public String toString() {
      return "IsEven(long)";
    }
  };

}
