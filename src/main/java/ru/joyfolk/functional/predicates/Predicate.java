package ru.joyfolk.functional.predicates;

import ru.joyfolk.functional.Function1;

public interface Predicate<T> extends Function1<Boolean, T> {
  Boolean apply(T arg);
}