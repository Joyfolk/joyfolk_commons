package ru.joyfolk.functional.predicates;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

class Xor<T> implements Predicate<T> {
  private Predicate<? super T> arg0;
  private Predicate<? super T> arg1;

  Xor(Predicate<? super T> arg0, Predicate<? super T> arg1) {
    assert arg0 != null : "Предикат arg0 не должен быть null";
    assert arg1 != null : "Предикат arg1 не должен быть null";
    this.arg0 = arg0;
    this.arg1 = arg1;
  }

  @Override
  public Boolean apply(T val) {
    return arg0.apply(val) != arg1.apply(val);
  }

  @Override
  public String toString() {
    return "Xor(" + arg0 + ","  + arg1 + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(Xor.class, arg0, arg1);
  }


  @Override
  public boolean equals(Object o) {
    return o instanceof Xor
        && Equals.isEquals(arg0, ((Xor) o).arg0)
        && Equals.isEquals(arg1, ((Xor) o).arg1);
  }

}