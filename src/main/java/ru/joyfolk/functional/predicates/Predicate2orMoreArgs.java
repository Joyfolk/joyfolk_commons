package ru.joyfolk.functional.predicates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class Predicate2orMoreArgs<T> implements Predicate<T> {
  protected List<Predicate<? super T>> args = new ArrayList<Predicate<? super T>>();

  protected Predicate2orMoreArgs(Predicate<? super T> arg0, Predicate<? super T> arg1, Predicate<? super T>... others) {
    assert arg0 != null : "Предикат arg0 не должен быть null";
    assert arg1 != null : "Предикат arg1 не должен быть null";
    args.add(arg0);
    args.add(arg1);
    args.addAll(Arrays.asList(others));
  }

  abstract public Boolean apply(final T val);
}