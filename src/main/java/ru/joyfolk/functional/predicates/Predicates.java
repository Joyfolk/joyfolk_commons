package ru.joyfolk.functional.predicates;

import ru.joyfolk.functional.predicates.simple.In;
import ru.joyfolk.functional.predicates.simple.InstanceOf;
import ru.joyfolk.functional.predicates.simple.IsEquals;
import ru.joyfolk.functional.predicates.simple.IsNull;

import java.util.Collection;


public class Predicates {
  public static <T> Predicate<T> and(Predicate<? super T> arg0, Predicate<? super T> arg1, Predicate<? super T>... others) {
    return new And<T>(arg0, arg1, others);
  }

  public static <T> Predicate<T> or(Predicate<? super T> arg0, Predicate<? super T> arg1, Predicate<? super T>... others) {
    return new Or<T>(arg0, arg1, others);
  }

  public static <T> Predicate<T> xor(Predicate<? super T> arg0, Predicate<? super T> arg1) {
    return new Xor<T>(arg0, arg1);
  }

  public static <T> Predicate<T> not(Predicate<? super T> arg0) {
    return new Not<T>(arg0);
  }

  public static <T> Predicate<T> in(Collection<? extends T> col) {
    return new In<T>(col);
  }

  public static <T> Predicate<T> notIn(Collection<? extends T> col) {
    return not(new In<T>(col));
  }

  public static <T> Predicate<T> isNull() {
    return new IsNull<T>();
  }

  public static <T> Predicate<T> isNotNull() {
    return not(new IsNull<T>());
  }

  public static Predicate<?> constTrue() {
    return predTrue;
  }

  private static final True<?> predTrue = new True<Object>();

  public static Predicate<?> constFalse() {
    return predFalse;
  }

  private static final False<?> predFalse = new False<Object>();

  public static <T> InstanceOf<T> instanceOf(Class<T> clss) {
    return new InstanceOf<T>(clss);
  }

  public static <T> Predicate<T> isEquals(T val) {
    return new IsEquals<T>(val);
  }

  public static <T> Predicate<T> isNotEquals(T val) {
    return not(isEquals(val));
  }
}
