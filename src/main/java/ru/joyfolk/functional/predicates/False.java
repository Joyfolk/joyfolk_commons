package ru.joyfolk.functional.predicates;

class False<T> implements Predicate<T> {
  @Override
  public Boolean apply(T arg) {
    return false;
  }

  @Override
  public String toString() {
    return "False()";
  }

}
