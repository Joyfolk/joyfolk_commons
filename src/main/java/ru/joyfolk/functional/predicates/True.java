package ru.joyfolk.functional.predicates;

 class True<T> implements Predicate<T> {
  @Override
  public Boolean apply(T arg) {
    return true;
  }

  @Override
   public String toString() {
    return "True()";
  }
}
