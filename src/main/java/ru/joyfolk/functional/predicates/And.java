package ru.joyfolk.functional.predicates;

import ru.joyfolk.collections.Collections2;
import ru.joyfolk.functional.Function2;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;
import ru.joyfolk.strings.Strings;


class And<T> extends Predicate2orMoreArgs<T> {
  And(Predicate<? super T> arg0, Predicate<? super T> arg1, Predicate<? super T>... others) {
    super(arg0, arg1, others);
  }

  @Override
  public Boolean apply(final T val) {
    return Collections2.accumulate(args, true, new Function2<Boolean, Boolean, Predicate<? super T>>() {
      public Boolean apply(Boolean arg1, Predicate<? super T> pred) {
        return arg1 && pred.apply(val);
      }
    });
  }

  @Override
  public String toString() {
    return "And(" + Strings.join(Strings.COMMA_SEPARATOR, args) + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(And.class, args.hashCode());
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof And && Equals.isEquals(args, ((And) o).args);
  }
}