package ru.joyfolk.functional.predicates;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

class Not<T> implements Predicate<T> {
  Predicate<? super T> arg;

  Not(Predicate<? super T> arg) {
    assert arg != null : "Предикат arg не должен быть null";
    this.arg = arg;
  }

  public Boolean apply(T val) {
    return !arg.apply(val);
  }

  @Override
  public String toString() {
    return "Not(" + arg + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(And.class, arg.hashCode());
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Not && Equals.isEquals(arg, ((Not) o).arg);
  }

}