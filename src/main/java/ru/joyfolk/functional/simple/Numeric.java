package ru.joyfolk.functional.simple;

import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.Function2;

import java.math.BigDecimal;
import java.math.BigInteger;


public class Numeric {
  public static Function2<Integer, Integer, Integer> getSumInt() {
    return summInt;
  }

  public static Function2<Long, Long, Long> getSumLong() {
    return summLong;
  }

  public static Function2<Double, Double, Double> getSumDouble() {
    return summDouble;
  }

  public static Function2<Float, Float, Float> getSumFloat() {
    return summFloat;
  }

  public static Function2<BigInteger, BigInteger, BigInteger> getSumBigInt() {
    return summBigInt;
  }

  public static Function2<BigDecimal, BigDecimal, BigDecimal> getSumBigDec() {
    return summBigDec;
  }

  public static Function1<Integer, Integer> getIncInt() {
    return incInt;
  }

  public static Function1<Integer, Integer> getDecInt() {
    return decInt;
  }

  public static Function2<Integer, Integer, Integer> getMultInt() {
    return multInt;
  }

  private final static Function2<Integer, Integer, Integer> summInt = new Function2<Integer, Integer, Integer>() {
    @Override
    public Integer apply(Integer arg1, Integer arg2) {
      return arg1 + arg2;
    }

    @Override
    public String toString() {
      return "Summ(int, int)";
    }
  };

  private final static Function2<Long, Long, Long> summLong = new Function2<Long, Long, Long>() {
    @Override
    public Long apply(Long arg1, Long arg2) {
      return arg1 + arg2;
    }

    @Override
    public String toString() {
      return "Summ(long, long)";
    }

  };

  private final static Function2<Double, Double, Double> summDouble = new Function2<Double, Double, Double>() {
    @Override
    public Double apply(Double arg1, Double arg2) {
      return arg1 + arg2;
    }

    @Override
    public String toString() {
      return "Summ(double, double)";
    }

  };

  private final static Function2<Float, Float, Float> summFloat = new Function2<Float, Float, Float>() {
    @Override
    public Float apply(Float arg1, Float arg2) {
      return arg1 + arg2;
    }

    @Override
    public String toString() {
      return "Summ(float, float)";
    }

  };

  private final static Function2<BigInteger, BigInteger, BigInteger> summBigInt = new Function2<BigInteger, BigInteger, BigInteger>() {
    @Override
    public BigInteger apply(BigInteger arg1, BigInteger arg2) {
      return arg1.add(arg2);
    }

    @Override
    public String toString() {
      return "Summ(BigInteger, BigInteger)";
    }
  };

  private final static Function2<BigDecimal, BigDecimal, BigDecimal> summBigDec = new Function2<BigDecimal, BigDecimal, BigDecimal>() {
    @Override
    public BigDecimal apply(BigDecimal arg1, BigDecimal arg2) {
      return arg1.add(arg2);
    }

    @Override
    public String toString() {
      return "Summ(BigDecimal, BigDecimal)";
    }
  };


  private final static Function1<Integer, Integer> incInt = new Function1<Integer, Integer>() {
    @Override
    public Integer apply(Integer arg1) {
      return arg1 + 1;
    }

    @Override
    public String toString() {
      return "Inc(int)";
    }
  };

  private final static Function1<Integer, Integer> decInt = new Function1<Integer, Integer>() {
    @Override
    public Integer apply(Integer arg1) {
      return arg1 - 1;
    }

    @Override
    public String toString() {
      return "Dec(int)";
    }
  };

  private final static Function2<Integer, Integer, Integer> multInt = new Function2<Integer, Integer, Integer>() {
    @Override
    public Integer apply(Integer arg1, Integer arg2) {
      return arg1 * arg2;
    }

    @Override
    public String toString() {
      return "Mult(int, int)";
    }
  };
}