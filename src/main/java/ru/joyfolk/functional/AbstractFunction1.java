package ru.joyfolk.functional;

public abstract class AbstractFunction1<R, T1> implements Function1<R, T1> {
	public AbstractFunction0<R> bind1st(T1 val) {
		return Bind.bind1st(this, val);
	}
	
	public <T2> AbstractFunction2<R, T1, T2> bindUp1st() {
		return Bind.bindUp1st(this);
	}
	
	public <T2> AbstractFunction2<R, T2, T1> bindUp2nd() {
		return Bind.bindUp2nd(this);
	}
	
	public <R1> AbstractFunction1<R1, T1> compose(Function1<R1, R> fun) {
	  return Compose.compose(this, fun);
	}
	
	
	
}
