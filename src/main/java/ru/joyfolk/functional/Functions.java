package ru.joyfolk.functional;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

import java.util.Map;
import java.util.concurrent.Callable;

public class Functions {
  public static <R> Function0<R> constant(final R value) {
    return new Constant<R>(value);
  }

  public static <R, T extends R> Function1<R, T> identity() {
    return new Identity<R, T>();
  }

  public static Function1<String, Object> toStringFunction() {
    return toStringFun;
  }

  public static Function1<Integer, Object> hashCodeFunction() {
    return hashCode;
  }

  public static Function1<Class, Object> toClassFunction() {
    return toClass;
  }

  public static <R> Function0<R> lazy(final Function0<R> fun) {
    assert fun != null: "Функция не должна быть null";
    return new Function0<R>() {
      volatile R val = null;
      final Object lock = new Object();

      void checkVal() {
        if(val == null) {
          synchronized (lock) {
            if(val == null)
              val = fun.apply();
          }
        }
      }

      @Override
      public R apply() {
        checkVal();
        return val;
      }

      @Override
      public String toString() {
        return "Lazy(" + fun + ")";
      }
    };
  }


  static class Constant<R> implements Function0<R> {
    private final R val;

    public Constant(R val) {
      this.val = val;
    }

    @Override
    public R apply() {
      return val;
    }

    public R getConstant() {
      return val;
    }

    @Override
    public String toString() {
      return "Constant(" + val + ")";
    }

    @Override
    public boolean equals(Object o) {
      return o instanceof Constant && Equals.isEquals(val, ((Constant) o).getConstant());
    }

    @Override
    public int hashCode() {
      return HashCode.hashCode(val);
    }
  }

  static class Identity<R, T extends R> implements Function1<R, T> {
    @Override
    public R apply(T arg) {
      return arg;
    }

    @Override
    public String toString() {
      return "Identity()";
    }
  }


  private static final Function1<String, Object> toStringFun = new Function1<String, Object>() {
    @Override
    public String apply(Object arg) {
      return arg.toString();
    }

    @Override
    public String toString() {
      return "ToString()";
    }
  };

  private static final Function1<Integer, Object> hashCode = new Function1<Integer, Object>() {
    @Override
    public Integer apply(Object arg) {
      return arg.hashCode();
    }

    @Override
    public String toString() {
      return "hashCode()";
    }
  };

  private static final Function1<Class, Object> toClass = new Function1<Class, Object>() {
    @Override
    public Class apply(Object arg) {
      return arg.getClass();
    }

    @Override
    public String toString() {
      return "toClass()";
    }
  };

  private static <R, T> Function1<R, T> fromMap(Map<T, R> map, R defaultValue) {
    return new MapFunction<R, T>(map, defaultValue);
  }

  private static <R, T> Function1<R, T> fromMap(Map<T, R> map) {
    return new MapFunction<R, T>(map, null);
  }
  
  public static <R, U, T extends Map.Entry<R, U>> Function1<R, T> mapKey(Map<R, U> map) {
  	return new Function1<R, T>() {
			@Override
      public R apply(T arg) {
	      return arg.getKey();
      }
  		
  	};
  }

  public static <R, U, T extends Map.Entry<U, R>> Function1<R, T> mapValue(Map<U, R> map) {
  	return new Function1<R, T>() {
			@Override
      public R apply(T arg) {
	      return arg.getValue();
      }  		
  	};
  }
  
  static class MapFunction<R, T> implements Function1<R, T> {
    final Map<T, R> map;
    final R defaultValue;

    MapFunction(Map<T, R> map, R defaultValue) {
      this.map = map;
      this.defaultValue = defaultValue;
    }

    @Override
    public R apply(T arg) {
      R ret = map.get(arg);
      return ret != null? ret: defaultValue;
    }

    @Override
    public String toString() {
      return "MapFunction(" + map + ", " + defaultValue + ")";
    }

    @Override
    public boolean equals(Object o) {
      if(o instanceof MapFunction) {
        MapFunction m = (MapFunction) o;
        return Equals.isEquals(defaultValue, m.defaultValue)
            && Equals.isEquals(map, m.map);
      }
      else
        return false;
    }

    @Override
    public int hashCode() {
      return HashCode.hashCode(defaultValue, map);
    }
  }

  public static <V> Callable<V> asCallable(final Function0<V> fun) {
    return new Callable<V>() {
      @Override
      public V call() throws Exception {
        return fun.apply();
      }
    };
  }
  
}
