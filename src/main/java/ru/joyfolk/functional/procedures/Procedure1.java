package ru.joyfolk.functional.procedures;

import ru.joyfolk.functional.Function1;

abstract public class Procedure1<T> implements Function1<Void, T> {
  protected abstract void execute(T arg);

  @Override
  final public Void apply(T arg) {
    execute(arg);
    return null;
  }

  @Override
  public String toString() {
    return "Procedure1()";
  }

}