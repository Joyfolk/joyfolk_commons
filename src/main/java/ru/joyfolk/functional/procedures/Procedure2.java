package ru.joyfolk.functional.procedures;

import ru.joyfolk.functional.Function2;

abstract public class Procedure2<T1, T2> implements Function2<Void, T1, T2> {
  protected abstract void execute(T1 arg1, T2 arg2);

  @Override
  final public Void apply(T1 arg1, T2 arg2) {
    execute(arg1, arg2);
    return null;
  }

  @Override
  public String toString() {
    return "Procedure2()";
  }

}