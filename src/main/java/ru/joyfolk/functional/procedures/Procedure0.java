package ru.joyfolk.functional.procedures;

import ru.joyfolk.functional.Function0;

public abstract class Procedure0 implements Function0<Void>, Runnable {
  protected abstract void execute();

  @Override
  final public Void apply() {
    execute();
    return null;
  }

  @Override
  public String toString() {
    return "Procedure0()";
  }

  @Override
  final public void run() {
    execute();
  }
}
