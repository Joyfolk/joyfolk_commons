package ru.joyfolk.functional.procedures;

import ru.joyfolk.functional.Function3;

abstract public class Procedure3<T1, T2, T3> implements Function3<Void, T1, T2, T3> {
  protected abstract void execute(T1 arg1, T2 arg2, T3 arg3);

  @Override
  final public Void apply(T1 arg1, T2 arg2, T3 arg3) {
    execute(arg1, arg2, arg3);
    return null;
  }

  @Override
  public String toString() {
    return "Procedure3()";
  }

}