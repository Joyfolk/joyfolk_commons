package ru.joyfolk.functional.procedures.simple;

import ru.joyfolk.functional.procedures.Procedure1;

import java.util.Collection;

public class Collect<T> extends Procedure1<T> {
  private Collection<? super T> collection;
  private Collect(Collection<? super T> collection) {
    assert collection != null: "Коллекция не должна быть null";
    this.collection = collection;
  }

  @Override
  final protected void execute(T arg) {
    collection.add(arg);
  }

  public static <T> Collect<T> into(Collection<? super T> collection) {
    return new Collect<T>(collection);
  }

  @Override
  public String toString() {
    return "Collect(" + collection.toString() + ")";
  }

}