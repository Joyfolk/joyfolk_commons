package ru.joyfolk.functional.procedures.simple;

import ru.joyfolk.functional.Function0;

public class Count implements Function0<Integer> {
  private int count = 0;

  @Override
  public Integer apply() {
    return ++count;
  }

  public int getCount() {
    return count;
  }

  @Override
  public String toString() {
    return "Count()";
  }

  
}