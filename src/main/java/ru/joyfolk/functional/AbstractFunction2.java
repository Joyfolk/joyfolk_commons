package ru.joyfolk.functional;

public abstract class AbstractFunction2<R, T1, T2> implements Function2<R, T1, T2> {
	public AbstractFunction1<R, T2> bind1st(T1 val) {
		return Bind.bind1st(this, val);
	}
	
	public AbstractFunction1<R, T1> bind2nd(T2 val) {
		return Bind.bind2nd(this, val);
	}	
	
	public <T3> AbstractFunction3<R, T3, T1, T2> bindUp1st() {
		return Bind.bindUp1st(this);
	}
	
	public <T3> AbstractFunction3<R, T1, T3, T2> bindUp2nd() {
		return Bind.bindUp2nd(this);
	}

	public <T3> AbstractFunction3<R, T1, T2, T3> bindUp3rd() {
		return Bind.bindUp3rd(this);
	}
	
	public <R1> AbstractFunction2<R1, T1, T2> compose(Function1<R1, R> fun) {
	  return Compose.compose(this, fun);
	}	

	public <T3> AbstractFunction2<R, T3, T2> bind1st(Function1<T1, T3> fun) {
	  return Bind.bind1st(fun, this);
	}	

	public <T3> AbstractFunction2<R, T1, T3> bind2nd(Function1<T2, T3> fun) {
	  return Bind.bind2nd(fun, this);
	}	
	
}
