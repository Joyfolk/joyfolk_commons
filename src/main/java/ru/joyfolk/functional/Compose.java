package ru.joyfolk.functional;

import ru.joyfolk.functional.predicates.Predicate;

public class Compose {
  public static <R, T1> AbstractFunction0<R> compose(final Function0<? extends T1> fun1, final Function1<? extends R, ? super T1> fun2) {
    return new AbstractFunction0<R>() {
      @Override
      public R apply() {
        return fun2.apply(fun1.apply());
      }

      @Override
      public String toString() {
        return "Compose0(" + fun1.toString() + "=>" + fun2.toString() + ")";
      }
    };
  }


  public static <R, T1, T2> AbstractFunction1<R, T1> compose(final Function1<? extends T2, ? super T1> fun1, final Function1<? extends R, ? super T2> fun2) {
    return new AbstractFunction1<R, T1>() {
      @Override
      public R apply(T1 arg) {
        return fun2.apply(fun1.apply(arg));
      }

      @Override
      public String toString() {
        return "Compose1(" + fun1.toString() + "=>" + fun2.toString() + ")";
      }
    };
  }

  public static <R, T1, T2, T3> AbstractFunction2<R, T1, T2> compose(final Function2<? extends T3, ? super T1, ? super T2> fun1, final Function1<? extends R, ? super T3> fun2) {
    return new AbstractFunction2<R, T1, T2>() {
      @Override
      public R apply(T1 arg1, T2 arg2) {
        return fun2.apply(fun1.apply(arg1, arg2));
      }

      @Override
      public String toString() {
        return "Compose2(" + fun1.toString() + "=>" + fun2.toString() + ")";
      }
    };
  }

  public static <R, T1, T2, T3, T4> AbstractFunction3<R, T1, T2, T3> compose(final Function3<? extends T4, ? super T1, ? super T2, ? super T3> fun1, final Function1<? extends R, ? super T4> fun2) {
    return new AbstractFunction3<R, T1, T2, T3>() {
      @Override
      public R apply(T1 arg1, T2 arg2, T3 arg3) {
        return fun2.apply(fun1.apply(arg1, arg2, arg3));
      }

      @Override
      public String toString() {
        return "Compose3(" + fun1.toString() + "=>" + fun2.toString() + ")";
      }
    };
  }

  public static <T> Predicate<T> predicate(final Function1<Boolean, ? super T> fun) {
    return new Predicate<T>() {
      @Override
      public Boolean apply(T arg) {
        return fun.apply(arg);
      }

      @Override
      public String toString() {
        return "Predicate(" + fun.toString() + ")";
      }
    };
  }
}
