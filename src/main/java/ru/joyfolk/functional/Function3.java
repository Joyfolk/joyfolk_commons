package ru.joyfolk.functional;

public interface Function3<R, T1, T2, T3> {
  R apply(T1 arg0, T2 arg1, T3 arg2);
}