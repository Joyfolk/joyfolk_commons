package ru.joyfolk.concurent.actors;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

public class ActorFactory {
	private final Executor executor;
	private final ConcurrentLinkedQueue<Actor> actors = new ConcurrentLinkedQueue<Actor>();
	
	public ActorFactory(Executor executor) {
		this.executor = executor;
		executor.execute(controlTask);
	}

	private final Callable<Void> myRunnable = new Callable<Void>() {
		public Void call() {
	    while(!controlTask.isCancelled()) {
	    	Actor actor = actors.poll();
	    	if(actor != null) 
	    		executor.execute(actor);  // обратно в очередь он добавится самостоятельно	    	
	    }	    
	    return null;
    }		
	};
	
	private final FutureTask<Void> controlTask = new FutureTask<Void>(myRunnable);
	
	public void cancel(boolean mayInterruptIfRunning) {
		controlTask.cancel(mayInterruptIfRunning);
	}
	
	public boolean isCanceled() {
		return controlTask.isCancelled();
	}
	
	private static class MethodRequest {
		private final Object proxy;
		private final Method method;
		private final Object[] args;
		
		public MethodRequest(Object proxy, Method method, Object[] args) {
			this.proxy = proxy;
			this.method = method;
			this.args = args;
		}
		
		public final Object invoke() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			return method.invoke(proxy, args);
		}
	}
	
	private class Actor implements Runnable {
		final ConcurrentLinkedQueue<MethodRequest> queue = new ConcurrentLinkedQueue<MethodRequest>();
		
		public Actor() {		
			actors.add(this);			
		}
		
		@Override
    public void run() {
	    MethodRequest request = queue.poll();
	    if(request != null)
	      try {
	        request.invoke();
        } 
	    	catch (Throwable e) {
	        throw new ActorMethodRequestException(e);
        }
	    actors.add(this);
    }
		
		public void addToMailbox(MethodRequest request) {
			queue.add(request);
		}
	}
	
	@SuppressWarnings("unchecked")
  public <T> T makeActor(final T object, Class<T> interf) {
		final Actor actor = new Actor();		
		InvocationHandler handler = new InvocationHandler() {		
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        throw new UnsupportedOperationException("Not implemented yet!"); // todo: not implemented
//				if(!method.isAnnotationPresent(Synchronous.class)) {
//					method.setAccessible(true);
//					actor.addToMailbox(new MethodRequest(proxy, method, args));
//					return null;
//				}
//				else
//					return method.invoke(object, args);
			}
		};
		Object proxy = Proxy.newProxyInstance(interf.getClassLoader(), new Class<?>[]{ interf }, handler);
		return interf.cast(proxy);
	}
}
