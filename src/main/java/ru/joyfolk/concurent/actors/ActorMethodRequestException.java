package ru.joyfolk.concurent.actors;

public class ActorMethodRequestException extends RuntimeException {
  private static final long serialVersionUID = -5758199958942040295L;

  public ActorMethodRequestException() { super(); }
  public ActorMethodRequestException(String cause) { super(cause); }
  public ActorMethodRequestException(String cause, Throwable e) { super(cause, e); }
  public ActorMethodRequestException(Throwable e) { super(e); }
  
  @Override
  public String toString() {
  	return "ActorMethodRequestException(" + getCause() + ")";
  }
}
