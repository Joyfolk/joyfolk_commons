package ru.joyfolk.concurent;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

public interface ListenableFuture<T> extends Future<T> {
  void addFutureListener(Runnable listener, Executor listenerExecutor);
}
