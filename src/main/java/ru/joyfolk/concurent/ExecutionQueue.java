package ru.joyfolk.concurent;

import ru.joyfolk.general.tuples.Tuple2;
import ru.joyfolk.general.tuples.Tuples;

import java.util.Queue;
import java.util.concurrent.*;

public class ExecutionQueue implements Runnable, Callable<Void>, Future<Void> {
  private final FutureTask<Void> myTask = new FutureTask<Void>(this);
  private final Queue<Tuple2<Executor, Runnable>> tasks = new ConcurrentLinkedQueue<Tuple2<Executor, Runnable>>();

  public void add(Executor executor, Runnable runnable) {
    tasks.add(Tuples.t(executor, runnable));
  }

  public void add(Executor executor, Callable callable) {
    add(executor, ConcurentUtils.asRunnable(callable));
  }

  public void remove(Executor executor, Runnable runnable) {
    tasks.remove(Tuples.t(executor, runnable));
  }

  public void clear() {
    tasks.clear();
  }

  @Override
  public Void call() {
    while(!myTask.isCancelled() && !tasks.isEmpty()) {
      Tuple2<Executor, Runnable> task = tasks.poll();
      task.first().execute(task.second());
    }
    return null;
  }

  @Override
  final public void run() {
    call();
  }

  @Override
  public boolean cancel(boolean mayInterruptIfRunning) {
    return myTask.cancel(mayInterruptIfRunning);
  }

  @Override
  public boolean isCancelled() {
    return myTask.isCancelled();
  }

  @Override
  public boolean isDone() {
    return myTask.isDone();
  }

  @Override
  public Void get() throws InterruptedException, ExecutionException {
    return myTask.get();
  }

  @Override
  public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
    return myTask.get(timeout, unit);
  }
}
