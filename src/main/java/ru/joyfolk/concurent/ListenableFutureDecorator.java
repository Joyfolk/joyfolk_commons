package ru.joyfolk.concurent;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

public class ListenableFutureDecorator<T> extends FutureDecorator<T, ListenableFutureTask<T>> implements ListenableFuture<T> {
  public ListenableFutureDecorator(Future<T> delegate) {
    super(new ListenableFutureTask(ConcurentUtils.asCallable(delegate)));
  }

  @Override
  public void addFutureListener(Runnable listener, Executor listenerExecutor) {
    getDelegate().addFutureListener(listener, listenerExecutor);
  }

  public void removeFutureListener(Runnable listener, Executor listenerExecutor) {
    getDelegate().removeFutureListener(listener, listenerExecutor);
  }
}
