package ru.joyfolk.concurent;

import ru.joyfolk.functional.Function1;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class Futures {
  public static <V> ListenableFuture<V> makeListenable(Future<V> future) {
    return new ListenableFutureDecorator(future);
  }

  public static <R, T> FutureTask<R> chain(Future<T> source, final Function1<? extends R, ? super T> function) {
    final FutureCallable<T> callable = ConcurentUtils.asCallable(source);
    return new FutureTask(new Callable<R>() {
      @Override
      public R call() throws Exception {
        return function.apply(callable.call());
      }
    });
  }


  public static <R, T> ListenableFutureTask<R> chainListenable(ListenableFutureTask<T> source, final Function1<? extends R, ? super T> function) {
//    return new ListenableFutureTask<R>(source.g);
    return null;
  }


}
