package ru.joyfolk.concurent;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class FutureCallable<T> implements Callable<T> {
  private final Future<? extends T> future;
  private Timeout timeout;

  public FutureCallable(Future<? extends T> future) {
    this(future, null);
  }

  public void setTimeout(long timeout, TimeUnit unit) {
    this.timeout = new Timeout(timeout, unit);
  }

  public FutureCallable(Future<? extends T> future, Timeout timeout) {
    this.future = future;
    this.timeout = timeout;
  }

  @Override
  public T call() throws Exception {
    T val;
    if(timeout == null)
     val = future.get();
    else
     val = future.get(timeout.timeout, timeout.unit);
    return val;
  }

  static class Timeout {
    public final long timeout;
    public final TimeUnit unit;
    public Timeout(long timeout, TimeUnit unit) {
      this.timeout = timeout;
      this.unit = unit;
    }
  }
}
