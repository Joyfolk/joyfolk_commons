package ru.joyfolk.concurent;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class ConcurentUtils {
  public static Runnable asRunnable(final Callable<?> callable) {
    class RunnableWrapper implements Runnable {
      @Override
      public void run() {
        try {
          callable.call();
        }
        catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }
    return new RunnableWrapper();
  }

  public static Callable<Void> asCallable(final Runnable runnable) {
    return new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        runnable.run();
        return null;
      }
    };
  }

  public static <T> FutureCallable<T> asCallable(final Future<T> future) {
    return new FutureCallable<T>(future);
  }

}
