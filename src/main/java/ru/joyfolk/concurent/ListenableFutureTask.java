package ru.joyfolk.concurent;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

public class ListenableFutureTask<T> extends FutureTask<T> implements ListenableFuture<T> {
  public ListenableFutureTask(Callable<T> callable) {
    super(callable);
  }

  public ListenableFutureTask(Runnable runnable, T result) {
    super(runnable, result);
  }

  private final ExecutionQueue listeners = new ExecutionQueue();

  @Override
  final public void addFutureListener(Runnable listener, Executor listenerExecutor) {
    listeners.add(listenerExecutor, listener);
  }

  final public void removeFutureListener(Runnable listener, Executor listenerExecutor) {
    listeners.remove(listenerExecutor, listener);
  }

  @Override
  protected void done() {
    listeners.run();
  }
}
