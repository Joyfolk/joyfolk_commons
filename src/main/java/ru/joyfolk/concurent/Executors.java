package ru.joyfolk.concurent;

import javax.swing.*;
import java.util.concurrent.Executor;

public class Executors {
  private Executors() {}


  public static Executor getEDTExecutor() {
    return edtExecutor;
  }

  public static Executor getSameThreadExecutor() {
    return sameThreadExecutor;
  }

  private static final Executor sameThreadExecutor = new Executor() {
    @Override
    public void execute(Runnable command) {
      command.run();
    }
  };

  private static final Executor edtExecutor = new Executor() {
    @Override
    public void execute(Runnable command) {
      SwingUtilities.invokeLater(command);
    }
  };
}
