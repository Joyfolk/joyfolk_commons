package ru.joyfolk.general;

import ru.joyfolk.reflection.TypeRef;

import java.util.HashMap;
import java.util.Map;

public class TypesafeGeneric<Limit> {
  private final Map<TypeRef<? extends Limit>, Limit> values = new HashMap<TypeRef<? extends Limit>, Limit>();

    public <V extends Limit> V get(TypeRef<V> clss) {
      return clss.cast(values.get(clss));
    }

    public <V extends Limit> void put(TypeRef<V> clss, V data) {
      values.clear();
      values.put(clss, data);
    }

    public boolean isEmpty() {
      return values.isEmpty();
    }

    public Limit getUnsafe() {
      if(isEmpty())
        return null;
      else
        return values.values().iterator().next();
    }

    public void clear() {
      values.clear();
    }

    public TypeRef<? extends Limit> getType() {
      if(isEmpty())
        return null;
      else
        return values.keySet().iterator().next();
    }

    @Override
    public String toString() {
      return "TypesafeGeneric(" + getUnsafe() + ")";
    }

    @Override
    public int hashCode() {
      return HashCode.hashCode(TypesafeGeneric.class, getUnsafe());
    }

    @Override
    public boolean equals(Object o) {
      return o instanceof TypesafeGeneric
          && Equals.isEquals(getUnsafe(), ((TypesafeGeneric) o).getUnsafe());
    }
  }
