package ru.joyfolk.general;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.Function2;
import ru.joyfolk.strings.Strings;

import java.util.Arrays;
import java.util.Comparator;

public class Comparators {
  public static <T extends Comparable<? super T>> int compareNullsLast(T o1, T o2) {
    return compare(o1, o2, NullStrategy.NULL_LAST);
  }

  public static <T extends Comparable<? super T>> int compareNullsFirst(T o1, T o2) {
    return compare(o1, o2, NullStrategy.NULL_FIRST);
  }

  public static <T> int compareNullsLast(T o1, T o2, Comparator<? super T> comparator) {
    return compare(o1, o2, NullStrategy.NULL_LAST, comparator);
  }

  public static <T> int compareNullsFirst(T o1, T o2, Comparator<? super T> comparator) {
    return compare(o1, o2, NullStrategy.NULL_FIRST, comparator);
  }

  public static <T extends Comparable<? super T>> Comparator<T> comparable() {
    return new Comparator<T>() {
      @Override
      public int compare(T o1, T o2) {
        return o1.compareTo(o2);
      }
    };
  }

  public enum NullStrategy {
    NULL_FIRST {
      public int nullValue() { return -1; }
    },
    NULL_LAST {
    	public int nullValue() { return 1; }
    };
    public abstract int nullValue();
  }

  public static <T extends Comparable<? super T>> int compare(T o1, T o2, NullStrategy nullStrategy) {
    return compare(o1, o2, nullStrategy, (Comparator<? super T>) COMPARABLE);
  }

  public static <T> int compare(T o1, T o2, NullStrategy nullStrategy, Comparator<? super T> comparator) {
    if(o1 == o2)
      return 0;
    if(o1 == null)
      return nullStrategy.nullValue();
    if(o2 == null)       // workaround для int-ов
      return -nullStrategy.nullValue();
    return comparator.compare(o1, o2);
  }

  public static final Comparator<? extends Comparable<Object>> COMPARABLE = new Comparator<Comparable<Object>>() {
    @Override
    public int compare(Comparable<Object> o1, Comparable<Object> o2) {
      return o1.compareTo(o2);
    }

    @Override
    public String toString() {
      return "Comparator<Comparable>";
    }
  };

  public static final Comparator<Object> TOSTRING = new Comparator<Object>() {
    @Override
    public int compare(Object o1, Object o2) {
      return o1.toString().compareTo(o2.toString());
    }

    @Override
    public String toString() {
      return "Comparator(toString())";
    }
  };

  public static <T> Comparator<T> fromFunction(final Function2<Integer, ? super T, ? super T> fun) {
    return new Comparator<T>() {
      @Override
      public int compare(T o1, T o2) {
        return fun.apply(o1, o2);
      }

      @Override
      public String toString() {
        return "Comparator(" + fun +")";
      }
    };
  }

  public static <T> Comparator<T> comparable2() {
     return new Comparator<T>() {
       @Override
       public int compare(T o1, T o2) {
         return ((Comparable) o1).compareTo(o2);
       }
     };
   }

  public <T> Comparator<T> compareAll(final Comparator<? super T>... comparators) {
		return new Comparator<T>() {
			@Override
      public int compare(T o1, T o2) {
	      return Iterators.applyComparators(Arrays.asList(comparators).iterator(), o1, o2);
      };  	
      
      @Override
      public String toString() {
      	return "CompareAll(" + Strings.join(Strings.COMMA_SEPARATOR, (Object[]) comparators) + ")";
      }
		};
  }
  
  public static <T> CompareResult chain() {
  	return new CompareResult(0);
  }
  
  public final static class CompareResult {
  	final int res;
  	
  	CompareResult(int res) {
  		this.res = res;
  	}
  	
  	public int result() {
  		return res;
  	}
  	
  	public <T extends Comparable<? super T>> CompareResult compareNullsFirst(T o1, T o2) {
			return res == 0? new CompareResult(Comparators.compareNullsFirst(o1, o2)): this;			
  	}
  	
  	public <T extends Comparable<? super T>> CompareResult compareNullsLast(T o1, T o2) {
			return res == 0? new CompareResult(Comparators.compareNullsLast(o1, o2)): this;			
  	}
  	
  	public <T> CompareResult compareNullsFirst(T o1, T o2, Comparator<? super T> comparator) {
			return res == 0? new CompareResult(Comparators.compareNullsFirst(o1, o2, comparator)): this;			
  	}

  	public <T> CompareResult compareNullsLast(T o1, T o2, Comparator<? super T> comparator) {
			return res == 0? new CompareResult(Comparators.compareNullsLast(o1, o2, comparator)): this;			
  	} 
  	
  	@Override
  	public String toString() {
  		return "CompareResult(" + res + ")";
  	}
  }
}
