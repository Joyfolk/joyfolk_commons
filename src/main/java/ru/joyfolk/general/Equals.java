package ru.joyfolk.general;

public class Equals {
  public static <T> boolean isEquals(T o1, T o2) {
    return o1 == o2 || o1 != null && o1.equals(o2);
  }

  public static <T> boolean isNotEquals(T o1, T o2) {
    return !isEquals(o1, o2);
  }

}
