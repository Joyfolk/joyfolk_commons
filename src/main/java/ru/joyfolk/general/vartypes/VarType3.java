package ru.joyfolk.general.vartypes;


public class VarType3<T1, T2, T3> {
  private T1 value1;
  private T2 value2;
  private T3 value3;

  public enum Type { first, second, third }
  private Type type;

  private VarType3(T1 value1, T2 value2, T3 value3) {
    assert value1 != null && value2 == null && value3 == null
        || value1 == null && value2 != null && value3 == null
        || value1 == null && value2 == null && value3 != null;
    this.value1 = value1;
    this.value2 = value2;
    this.value3 = value3;
    this.type = value1 != null? Type.first: value2 == null? Type.second: Type.third;
  }

  public static <T1, T2, T3> VarType3<T1, T2, T3> create1(T1 value) {
    return new VarType3<T1, T2, T3>(value, null, null);
  }

  public static <T1, T2, T3> VarType3<T1, T2, T3> create2(T2 value) {
    return new VarType3<T1, T2, T3>(null, value, null);
  }

  public static <T1, T2, T3> VarType3<T1, T2, T3> create3(T3 value) {
    return new VarType3<T1, T2, T3>(null, null, value);
  }

  public T1 getValue1(){
    return value1;
  }

  public T2 getValue2() {
    return value2;
  }

  public T3 getValue3() {
    return value3;
  }

  public void setValue1(T1 value) {
    value3 = null;
    value2 = null;
    value1 = value;
    type = Type.first;
  }

  public void setValue2(T2 value) {
    value3 = null;
    value2 = value;
    value1 = null;
    type = Type.second;
  }

  public void setValue3(T3 value) {
    value3 = value;
    value2 = null;
    value1 = null;
    type = Type.third;
  }

  public Type getType() {
    return type;
  }
}