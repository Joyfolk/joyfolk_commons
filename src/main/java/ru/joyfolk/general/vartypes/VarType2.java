package ru.joyfolk.general.vartypes;

public class VarType2<T1, T2> {
  private T1 value1;
  private T2 value2;

  public enum Type { first, second }
  private Type type;

  private VarType2(T1 value1, T2 value2) {
    assert value1 != null && value2 == null || value1 == null && value2 != null;
    this.value1 = value1;
    this.value2 = value2;
    this.type = value1 != null? Type.first: Type.second;
  }

  public static <T1, T2> VarType2<T1, T2> create1(T1 value) {
    return new VarType2<T1, T2>(value, null);
  }

  public static <T1, T2> VarType2<T1, T2> create2(T2 value) {
    return new VarType2<T1, T2>(null, value);
  }

  public T1 getValue1(){
    return value1;
  }

  public T2 getValue2() {
    return value2;
  }

  public void setValue1(T1 value) {
    value2 = null;
    value1 = value;
    type = Type.first;
  }

  public void setValue2(T2 value) {
    value2 = value;
    value1 = null;
    type = Type.second;
  }

  public Type getType() {
    return type;
  }
}