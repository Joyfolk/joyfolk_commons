package ru.joyfolk.general;

import java.util.Arrays;

public class HashCode {
  public static int hashCode(Object... objects) {
    return Arrays.hashCode(objects);
  }
}
