package ru.joyfolk.general;

import java.util.HashMap;
import java.util.Map;

public class Typesafe<Limit>  {
  private final Map<Class<? extends Limit>, Limit> values = new HashMap<Class<? extends Limit>, Limit>();

  public <V extends Limit> V get(Class<V> clss) {
    return clss.cast(values.get(clss));
  }

  public <V extends Limit> void put(Class<V> clss, V data) {
    values.clear();
    values.put(clss, data);
  }

  public boolean isEmpty() {
    return values.isEmpty();
  }

  public Limit getUnsafe() {
    if(isEmpty())
      return null;
    else
      return values.values().iterator().next();
  }

  public void clear() {
    values.clear();
  }

  public Class<? extends Limit> getType() {
    if(isEmpty())
      return null;
    else
      return values.keySet().iterator().next(); 
  }

  @Override
  public String toString() {
    return "Typesafe(" + getUnsafe() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(Typesafe.class, getUnsafe());
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Typesafe
        && Equals.isEquals(getUnsafe(), ((Typesafe) o).getUnsafe());
  }
}
