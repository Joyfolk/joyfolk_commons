package ru.joyfolk.general;

public interface ExceptionHandler {
  void handle(Throwable e);
}
