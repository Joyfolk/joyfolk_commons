package ru.joyfolk.general;

public interface Decorator<T> {
  T getDelegate();
}
