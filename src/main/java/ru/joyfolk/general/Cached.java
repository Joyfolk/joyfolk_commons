package ru.joyfolk.general;

import ru.joyfolk.functional.Function0;

public class Cached<T> {
  private T object;
  private volatile boolean initialized = false;
  private final Object lock;
  private final Function0<? extends T> generator;
  private final ExceptionHandler exceptionHandler;

  public Cached(Function0<? extends T> generator) {
    this(generator, null);
  }

  public Cached(Function0<? extends T> generator, ExceptionHandler exceptionHandler) {
    this.lock = new Object();
    this.generator = generator;
    this.exceptionHandler = exceptionHandler;
  }

  public T get() {
    if(!initialized)
      synchronized(lock) {
        if(!initialized)
          try {
            object = generator.apply();
            initialized = true;
          }
          catch(Exception e) {
            if(exceptionHandler != null)
              exceptionHandler.handle(e);
            else
              e.printStackTrace();
          }
      }
    return object;
  }

  public void recache() {
    synchronized(lock) {
      try {
        object = generator.apply();
        initialized = true;
      }
      catch(Exception e) {
        exceptionHandler.handle(e);
      }
    }
  }
}
