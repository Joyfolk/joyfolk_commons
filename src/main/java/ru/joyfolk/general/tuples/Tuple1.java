package ru.joyfolk.general.tuples;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Tuple1<T1> extends Tuple {
  private final T1 first;

  public Tuple1(T1 first) {
    this.first = first;
  }

  public T1 first() {
    return first;
  }

  @Override
  public String toString() {
    return "Tuple(" + first() +  ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(first());
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Tuple1) {
      Tuple1 t = (Tuple1) o;
      return Equals.isEquals(first(), t.first());
    }
    else
      return false;
  }
  
  @Override
  int dimension() {
	return 1;
  }
  
  public static <T1> boolean match(Tuple1<T1> template, Tuple1<? extends T1> value) {
	return template.first() == null 
		|| Equals.isEquals(template.first(), value.first);	  
  }
}
