package ru.joyfolk.general.tuples;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Tuple4<T1, T2, T3, T4> extends Tuple3<T1, T2, T3> {
  private final T4 fourth;

  public Tuple4(T1 first, T2 second, T3 third, T4 fourth) {
    super(first, second, third);
    this.fourth = fourth;
  }

  public T4 fourth() {
    return fourth;
  }

  @Override
  public String toString() {
    return "Tuple(" + first() + ", " + second() + ", " + third() + ", " + fourth() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(first(), second(), third(), fourth());
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Tuple4) {
      Tuple4 t = (Tuple4) o;
      return Equals.isEquals(fourth(), t.fourth()) 
          && Equals.isEquals(third(), t.third())
          && Equals.isEquals(second(), t.second())
          && Equals.isEquals(first(), t.first());
    }
    else
      return false;
  }
  
  @Override
  int dimension() {
  	return 4;
  } 

  public static <T1, T2, T3, T4> boolean match(Tuple4<T1, T2, T3, T4> template, Tuple4<? extends T1, ? extends T2, ? extends T3, ? extends T4> value) {
		return Tuple3.match(template, value) 
		    && (template.fourth == null || Equals.isEquals(template.fourth, value.fourth));	  
  }    
  
}
