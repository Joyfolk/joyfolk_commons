package ru.joyfolk.general.tuples;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Tuple3<T1, T2, T3> extends Tuple2<T1, T2> {
  private final T3 third;

  public Tuple3(T1 first, T2 second, T3 third) {
    super(first, second);
    this.third = third;
  }

  public T3 third() {
    return third;
  }

  @Override
  public String toString() {
    return "Tuple(" + first() + ", " + second() + ", " + third() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(first(), second(), third());
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Tuple3) {
      Tuple3 t = (Tuple3) o;
      return Equals.isEquals(third(), t.third())
          && Equals.isEquals(second(), t.second())
          && Equals.isEquals(first(), t.first());
    }
    else
      return false;
  }
  
  @Override
  int dimension() {
  	return 3;
  }  
  
  public static <T1, T2, T3> boolean match(Tuple3<T1, T2, T3> template, Tuple3<? extends T1, ? extends T2, ? extends T3> value) {
		return Tuple2.match(template, value) 
		    && (template.third == null || Equals.isEquals(template.third, value.third));	  
  }    
}
