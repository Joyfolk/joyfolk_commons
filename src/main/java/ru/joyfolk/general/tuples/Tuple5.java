package ru.joyfolk.general.tuples;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Tuple5<T1, T2, T3, T4, T5> extends Tuple4<T1, T2, T3, T4> {
  private final T5 fifth;

  public Tuple5(T1 first, T2 second, T3 third, T4 fourth, T5 fifth) {
    super(first, second, third, fourth);
    this.fifth = fifth;
  }

  public T5 fifth() {
    return fifth;
  }

  @Override
  public String toString() {
    return "Tuple(" + first() + ", " + second() + ", " + third() + ", " + fourth() + "," + fifth() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(first(), second(), third(), fourth(), fifth());
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Tuple5) {
      Tuple5 t = (Tuple5) o;
      return Equals.isEquals(fifth(), t.fifth())
          && Equals.isEquals(fourth(), t.fourth())
          && Equals.isEquals(third(), t.third())
          && Equals.isEquals(second(), t.second())
          && Equals.isEquals(first(), t.first());
    }
    else
      return false;
  }
  
  @Override
  int dimension() {
  	return 5;
  } 
  
  public static <T1, T2, T3, T4, T5> boolean match(Tuple5<T1, T2, T3, T4, T5> template, Tuple5<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5> value) {
		return Tuple4.match(template, value) 
		    && (template.fifth == null || Equals.isEquals(template.fifth, value.fifth));	  
  }    
  
}
