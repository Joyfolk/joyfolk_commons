package ru.joyfolk.general.tuples;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Tuple2<T1, T2> extends Tuple1<T1> {
  private final T2 second;

  public Tuple2(T1 first, T2 second) {
    super(first);
    this.second = second;
  }

  public T2 second() {
    return second;
  }

  @Override
  public String toString() {
    return "Tuple(" + first() + ", " + second() + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(first(), second());
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Tuple3) {
      Tuple2 t = (Tuple2) o;
      return Equals.isEquals(second(), t.second())
          && Equals.isEquals(first(), t.first());
    }
    else
      return false;
  }
  
  @Override
  int dimension() {
	return 2;
  }  
  
  public static <T1, T2> boolean match(Tuple2<T1, T2> template, Tuple2<? extends T1, ? extends T2> value) {
	return Tuple1.match(template, value) 
	    && (template.second == null || Equals.isEquals(template.second, value.second));	  
  }  
}
