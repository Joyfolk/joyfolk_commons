package ru.joyfolk.general.tuples;

import java.util.AbstractList;
import java.util.List;

import ru.joyfolk.functional.Function1;

public class Tuples {
  public static <T1> Tuple1<T1> t(T1 val) {
    return new Tuple1<T1>(val);
  }

  public static <T1, T2> Tuple2<T1, T2> t(T1 val1, T2 val2) {
    return new Tuple2<T1, T2>(val1, val2);
  }

  public static <T1, T2, T3> Tuple3<T1, T2, T3> t(T1 val1, T2 val2, T3 val3) {
    return new Tuple3<T1, T2, T3>(val1, val2, val3);
  }

  public static <T1, T2, T3, T4> Tuple4<T1, T2, T3, T4> t(T1 val1, T2 val2, T3 val3, T4 val4) {
    return new Tuple4<T1, T2, T3, T4>(val1, val2, val3, val4);
  }

  public static <T1, T2, T3, T4, T5> Tuple5<T1, T2, T3, T4, T5> t(T1 val1, T2 val2, T3 val3, T4 val4, T5 val5) {
    return new Tuple5<T1, T2, T3, T4, T5>(val1, val2, val3, val4, val5);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public static <T extends Tuple> boolean match(T template, T value) {
		if(template instanceof Tuple5)
			return match5((Tuple5) template, (Tuple5) value);		
		if(template instanceof Tuple4)
			return match4((Tuple4) template, (Tuple4) value);
		if(template instanceof Tuple3)
			return match3((Tuple3) template, (Tuple3) value);
		if(template instanceof Tuple2)
			return match2((Tuple2) template, (Tuple2) value);  	
		if(template instanceof Tuple1)
			return match1((Tuple1) template, (Tuple1) value);
		throw new RuntimeException("Tuple type not supported " + template.getClass());
  }
  
  public static <T1> boolean match1(Tuple1<T1> template, Tuple1<? extends T1> value) {
  	return Tuple1.match(template, value);
  }
  
  public static <T1, T2> boolean match2(Tuple2<T1, T2> template, Tuple2<? extends T1, ? extends T2> value) {
	  return Tuple2.match(template, value);
  }

  public static <T1, T2, T3> boolean match3(Tuple3<T1, T2, T3> template, Tuple3<? extends T1, ? extends T2, ? extends T3> value) {
	  return Tuple3.match(template, value);
  }

  public static <T1, T2, T3, T4> boolean match4(Tuple4<T1, T2, T3, T4> template, Tuple4<? extends T1, ? extends T2, ? extends T3, ? extends T4> value) {
	  return Tuple4.match(template, value);
  }

  public static <T1, T2, T3, T4, T5> boolean match5(Tuple5<T1, T2, T3, T4, T5> template, Tuple5<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5> value) {
	  return Tuple5.match(template, value);
  }  
  
  public static <T1> Function1<T1, Tuple1<? extends T1>> first() {
  	return new Function1<T1, Tuple1<? extends T1>>() {
			@Override
      public T1 apply(Tuple1<? extends T1> arg) {
	      return arg.first();
      }
			
			@Override
			public String toString() {
				return "First()";
			}
  	};
  }
  
  public static <T2> Function1<T2, Tuple2<?, ? extends T2>> second() {
  	return new Function1<T2, Tuple2<?, ? extends T2>>() {
			@Override
      public T2 apply(Tuple2<?, ? extends T2> arg) {
	      return arg.second();
      }
			
			@Override
			public String toString() {
				return "Second()";
			}
  	};
  }
  	
  public static <T3> Function1<T3, Tuple3<?, ?, ? extends T3>> third() {
    	return new Function1<T3, Tuple3<?, ?, ? extends T3>>() {
  			@Override
        public T3 apply(Tuple3<?, ?, ? extends T3> arg) {
  	      return arg.third();
        }
  			
  			@Override
  			public String toString() {
  				return "Third()";
  			}
    	};  	
  }

	
  public static <T4> Function1<T4, Tuple4<?, ?, ?, ? extends T4>> fourth() {
    	return new Function1<T4, Tuple4<?, ?, ?, ? extends T4>>() {
  			@Override
        public T4 apply(Tuple4<?, ?, ?, ? extends T4> arg) {
  	      return arg.fourth();
        }
  			
  			@Override
  			public String toString() {
  				return "Fourth()";
  			}
    	};  	
  }
  
  public static <T5> Function1<T5, Tuple5<?, ?, ?, ?, ? extends T5>> fifth() {
  	return new Function1<T5, Tuple5<?, ?, ?, ?, ? extends T5>>() {
			@Override
      public T5 apply(Tuple5<?, ?, ?, ?, ? extends T5> arg) {
	      return arg.fifth();
      }
			
			@Override
			public String toString() {
				return "Fifth()";
			}
  	};  	
  }  
  
  @SuppressWarnings("rawtypes")
  public static Object getNth(Tuple t, int n) {
  	if(n == 0 && t instanceof Tuple1) 
  		return ((Tuple1) t).first();
  	else if(n == 1 && t instanceof Tuple2) 
  		return ((Tuple2) t).second();
  	else if(n == 2 && t instanceof Tuple3)
  		return ((Tuple3) t).third();
  	else if(n == 3 && t instanceof Tuple4)
  		return ((Tuple4) t).fourth();
  	else if(n == 4 && t instanceof Tuple5) 
  		return ((Tuple5) t).fifth();  
		throw new IllegalArgumentException();  	
  }
  
  public List<Object> asList(final Tuple t) {
  	return new AbstractList<Object>() {
			@Override
      public Object get(int index) {
	      return getNth(t, index);
      }

			@Override
      public int size() {
	      return t.dimension();
      }  		
  	};
  }
  
  public Object[] asArray(final Tuple t) {
  	return asList(t).toArray();
  }
}
