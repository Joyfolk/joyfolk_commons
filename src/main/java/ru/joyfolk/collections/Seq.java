package ru.joyfolk.collections;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.collections.iterators.SeqIterator;
import ru.joyfolk.functional.Function0;
import ru.joyfolk.functional.Functions;
import ru.joyfolk.general.HashCode;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Seq<T> extends AbstractCollection<T> {
  private final T head;
  private final Seq<T> tail;

  Seq(T head, Seq<T> tail) {
    this.head = head;
    this.tail = tail;
  }

  public Seq<T> append(T elem) {
    return new Seq<T>(elem, this);
  }

  public Seq<T> appendAll(Iterator<T> iter) {
    Seq<T> c = this;
    while(iter.hasNext())
      c = c.append(iter.next());
    return c;
  }

  public static <T> Seq<T> from(T... values) {
    Seq<T> c = nil();
    for(int i = values.length - 1; i >= 0; i--)
      c = c.append(values[i]);
    return c;
  }

  public static <T> Seq<T> from(Iterator<T> iter) {
    Seq<T> c = nil();    
    while(iter.hasNext())
      c = c.append(iter.next());
    return c;
  }

  public static <T> Seq<T> nil() {
    return (Nil<T>) nil;
  }

  public Seq<T> appendAll(Iterable<T> iterable) {
    return appendAll(iterable.iterator());
  }

  public T head() {
    return head;
  }

  public Seq<T> tail() {
    return tail;
  }

  public boolean isNil() {
    return false;
  }

  boolean isLastElem() {
    return tail.isNil();
  }

  @Override
  final public SeqIterator<T> iterator() {
    return new SeqIterator<T>(this);
  }

  @Override
  final public int size() {
    return Iterators.count(iterator());
  }

  @Override
  public String toString() {
    return head() + ":" + tail();
  }

  private Function0<Integer> hashCode = Functions.lazy(new Function0<Integer>() {
    @Override
    public Integer apply() {
      return HashCode.hashCode(Seq.class, head, tail);
    }
  });

  @Override
  public int hashCode() {
    return hashCode.apply();
  }

  @SuppressWarnings({"unchecked"})
  @Override
  public boolean equals(Object o) {
    return o instanceof Seq
        && Iterators.isEquals(iterator(), ((Iterable) o).iterator()); 
  }

  private static final Nil<?> nil = new Nil<Object>();

  private static final class Nil<T> extends Seq<T> {
    public Nil() {
      super(null, null);
    }

    @Override
    public boolean isNil() {
      return true;
    }

    @Override
    public Seq<T> tail() {
      throw new NoSuchElementException("Nil element");
    }

    @Override
    public T head() {
      throw new NoSuchElementException("Nil element");
    }

    @Override
    boolean isLastElem() {
      return true;
    }

    @Override
    public String toString() {
      return "Nil";
    }

  }


}
