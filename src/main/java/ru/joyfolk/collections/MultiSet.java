package ru.joyfolk.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface MultiSet<T> extends Set<T> {
	int count(T object);
	void add(T object, int count);
	void remove(T object, int count);
	void set(T object, int count);
	void removeAll(T object);
	Set<T> distinct();
	Set<Map.Entry<T, Integer>> entrySet();
	int distinctCount();
	
	void removeAllOccurence(Collection<?> collection);
	void retainAllOccurence(Collection<?> collection);
	
	
}
