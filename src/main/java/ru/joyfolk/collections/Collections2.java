package ru.joyfolk.collections;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.Function0;
import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.Function2;
import ru.joyfolk.functional.exceptions.InvocationException;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.tuples.Tuple2;
import ru.joyfolk.reflection.TypeRef;

import java.lang.reflect.InvocationTargetException;
import java.util.*;


public class Collections2 {

  public static <T> void removeIf(Iterable<? extends T> data, Predicate<? super T> predicate) {
    Iterators.removeIf(data.iterator(), predicate);
  }

  public static <T> void retainIf(Iterable<? extends T> data, Predicate<? super T> predicate) {
    Iterators.retainIf(data.iterator(), predicate);
  }

  public static <T> void foreach(Iterable<? extends T> data, Function1<?, ? super T> fun) {
    Iterators.foreach(data.iterator(), fun);
  }

  public static <R, T> void map(Iterable<? extends T> data, final Collection<? super R> target, final Function1<? extends R, ? super T> fun) {
    Iterators.map(data.iterator(), target, fun);
  }

  public static <R, T, C extends Collection<R>> C map(Iterable<? extends T> data, TypeRef<C> targetClss, final Function1<? extends R, ? super T> fun) {
    return Iterators.map(data.iterator(), targetClss, fun);
  }


  public static <R, T> List<R> mapToArray(Iterable<? extends T> data, final Function1<? extends R, ? super T> fun) {
    return Iterators.mapToArray(data.iterator(), fun);
  }

  public static <R, T> List<R> mapToList(Iterable<? extends T> data, final Function1<? extends R, ? super T> fun) {
    return Iterators.mapToList(data.iterator(), fun);
  }

  public static <R, T> R accumulate(Iterable<? extends T> data, R startValue, final Function2<R, R, T> fun) {
    return Iterators.accumulate(data.iterator(), startValue, fun);
  }

  public static <R> void fill(Collection<? super R> collection, int count, Function0<R> generator) {
    for(int i = 0; i < count; i++)
      collection.add(generator.apply());
  }

  public static <C extends Iterable<T>, T> Iterable<T> filter(final C iterable, final Predicate<? super T> predicate) {
    return new Iterable<T>() {
      @Override
      public Iterator<T> iterator() {
        return Iterators.applyFilter(iterable.iterator(), predicate);
      }
    };
  }

  public static <T extends Iterable<?>> int count(T iterable) {
    return Iterators.count(iterable.iterator());
  }

  public static <T, C extends Collection<? super T>> C collect(Iterable<? extends T> iterable, C collection) {
    return Iterators.collect(iterable.iterator(), collection);
  }

  public static <T> ArrayList<T> collectToArray(Iterable<? extends T> iterable) {
    return collect(iterable, new ArrayList<T>());
  }

  public static <T> LinkedList<T> collectToList(Iterable<? extends T> iterable) {
    return collect(iterable, new LinkedList<T>());
  }

  public static <T> HashSet<T> collectToSet(Iterable<? extends T> iterable) {
    return collect(iterable, new HashSet<T>());
  }
  public static <T, C extends Collection<? super T>> C collect(Iterable<? extends T> iterable, TypeRef<C> targetCollectionClss) {
    try {
      C col = targetCollectionClss.newInstance();
      return collect(iterable, col);
    }
    catch (NoSuchMethodException e) {
      throw new InvocationException(e);
    }
    catch (IllegalAccessException e) {
      throw new InvocationException(e);
    }
    catch (InvocationTargetException e) {
      throw new InvocationException(e);
    }
    catch (InstantiationException e) {
      throw new InvocationException(e);
    }
  }

  public static <K, V> HashMap<K, V> hashMap(Tuple2<K, V>... tuples) {
    return hashMap(Arrays.asList(tuples));
  }

  public static <K, V> HashMap<K, V> hashMap(Iterable<Tuple2<K, V>> tuples) {
    HashMap<K, V> ret = new HashMap<K, V>();
    for(Tuple2<K, V> t: tuples)
      ret.put(t.first(), t.second());
    return ret;
  }

  public static <K, V> HashMap<K, V> hashMap(Collection<V> source, final Function1<K, V> mapper) {
    List<Tuple2<K, V>> tuples = Collections2.mapToArray(source, new Function1<Tuple2<K, V>, V>() {
      @Override
      public Tuple2<K, V> apply(V arg) {
        return new Tuple2<K, V>(mapper.apply(arg), arg);
      }
    });
    return hashMap(tuples);
  }

  public static <K extends Comparable<? super K>, V> TreeMap<K, V> treeMap(Tuple2<K, V>... tuples) {
    TreeMap<K, V> ret = new TreeMap<K, V>();
    for(Tuple2<K, V> t: tuples)
      ret.put(t.first(), t.second());
    return ret;
  }

  public static <K, V> TreeMap<K, V> treeMap(Comparator<? super K> comparator, Tuple2<K, V>... tuples) {
    TreeMap<K, V> ret = new TreeMap<K, V>(comparator);
    for(Tuple2<K, V> t: tuples)
      ret.put(t.first(), t.second());
    return ret;
  }

  public static <K, V> Map<K, V> decorateWithDefaults(Map<K, V> map, final V defaultValue) {
	return new AbstractMapWrapper<K, V>(map) {
		@Override
		public V get(Object key) {
			V ret = getWrapped().get(key);
			return ret == null? defaultValue: ret;			
		}
	};	  
  }
  
  @SuppressWarnings("unchecked")
  public static <K, V extends List<?>> Map<K, V> decorateWithEmptyList(Map<K, V> map) {
	return decorateWithDefaults(map, (V) Collections.EMPTY_LIST);	  
  }	
  
  
}
