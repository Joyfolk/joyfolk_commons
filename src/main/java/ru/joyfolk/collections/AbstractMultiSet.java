package ru.joyfolk.collections;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import ru.joyfolk.functional.Bind;
import ru.joyfolk.functional.Functions;
import ru.joyfolk.functional.simple.Numeric;

public abstract class AbstractMultiSet<T> extends AbstractCollection<T> implements MultiSet<T> {
	private final Map<T, Integer> baseMap;
	
	protected AbstractMultiSet(Map<T, Integer> baseMap) {
		this.baseMap = baseMap;
	}

	@Override
  public int count(T object) {
	  Integer i = baseMap.get(object);
	  return nvl(i);	  
  }
	
	private static int nvl(Integer i) {
		return i == null? 0: i;
	}

	@Override
  public void add(T object, int count) {
	  set(object, count(object));
  }

	@Override
  public void remove(T object, int count) {
	  set(object, Math.max(count(object) - count, 0));
  }

	@Override
  public void set(T object, int count) {
	  baseMap.put(object, count);
  }

	@Override
  public void removeAll(T object) {
	  baseMap.remove(object);
  }

	@Override
  public Set<T> distinct() {
	  return baseMap.keySet();
  }

	@Override
  public Set<Map.Entry<T, Integer>> entrySet() {
	  return baseMap.entrySet();
  }

	@Override
  public int distinctCount() {
	  return baseMap.size();
  }

	@Override
  public void removeAllOccurence(Collection<?> collection) {
	  baseMap.keySet().removeAll(collection);
  }

	@Override
  public void retainAllOccurence(Collection<?> collection) {
	  baseMap.keySet().retainAll(collection);
  }

	@Override
  public Iterator<T> iterator() {
	  return new MultiSetIterator();
  }

	@Override
  public int size() {
	  return Collections2.accumulate(baseMap.entrySet(), (Integer) 0, Bind.bind2nd(Functions.mapValue(baseMap), Numeric.getSumInt()));
  }

	private class MultiSetIterator implements Iterator<T> {
		int remained = 0;
		Map.Entry<T, Integer> last;
		Iterator<Map.Entry<T, Integer>> baseIterator;
		
		public MultiSetIterator() {
			baseIterator = baseMap.entrySet().iterator();
		}
		
		@Override
    public boolean hasNext() {
	    return remained > 0 || baseIterator.hasNext();
    }

		@Override
    public T next() {			
	    if(remained == 0) {
	    	last = baseIterator.next();
		    remained = last.getValue();
	    }
	    if(remained > 0) {
	    	--remained;
	    	return last.getKey();
	    }
	    throw new NoSuchElementException();
    }

		@Override
    public void remove() {
	    if(last == null)
	    	throw new IllegalStateException();
	    if(last.getValue() > 1) {
	    	last.setValue(last.getValue() - 1);
	    }
	    else {
	    	baseIterator.remove();
	    	last = null;
	    }
    }
	}
}
