package ru.joyfolk.collections.iterators;

import java.util.NoSuchElementException;

public abstract class AbstractIterator<T> extends UnmodifiableIterator<T> {
  protected abstract T calcNext();

  private T next;
  private boolean firstStep = true;
  private boolean finished = false;

  protected final T onStop() {
    finished = true;
    return null;
  }

  @Override
  final public boolean hasNext() {
    onFirstStep();
    return !finished;
  }

  private void onFirstStep() {
    if(firstStep) {
      next = calcNext();
      firstStep = false;
    }
  }

  @Override
  final public T next() {
    onFirstStep();
    if(finished)
      throw new NoSuchElementException();
    T value = next;
    next = calcNext();
    return value;
  }
}
