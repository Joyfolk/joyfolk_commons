package ru.joyfolk.collections.iterators;

import ru.joyfolk.collections.Seq;

public class SeqIterator<T> extends AbstractIterator<T> {
  private Seq<T> current;

  public SeqIterator(Seq<T> current) {
    this.current = current;
  }

  @Override
  protected T calcNext() {
    if(current.isNil())
      return onStop();
    else {
      T value = current.head();
      current = current.tail();
      return value;
    }
  }
}
