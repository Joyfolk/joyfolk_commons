package ru.joyfolk.collections.iterators;

import ru.joyfolk.functional.predicates.Predicate;

import java.util.Iterator;

public class ExtractingIterator<T> extends AbstractIterator<T> {
  private final Iterator<T> source;
  private final Predicate<? super T> stopCondition;

  public ExtractingIterator(Iterator<T> source, Predicate<? super T> stopCondition) {
    this.source = source;
    this.stopCondition = stopCondition;
  }

  @Override
  protected T calcNext() {
    while(source.hasNext()) {
      T value = source.next();
      if(!stopCondition.apply(value))
        return value;
      else
        break;
    }
    return onStop();
  }
}