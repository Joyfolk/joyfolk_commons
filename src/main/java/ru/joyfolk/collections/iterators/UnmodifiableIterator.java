package ru.joyfolk.collections.iterators;

import java.util.Iterator;

public abstract class UnmodifiableIterator<T> implements Iterator<T> {
  @Override
  final public void remove() {
    throw new UnsupportedOperationException("Remove operation is not suported");
  }
}
