package ru.joyfolk.collections.iterators;

import ru.joyfolk.functional.predicates.Predicate;

import java.util.Iterator;

public class FilteringIterator<T> extends AbstractIterator<T> {
  private final Iterator<T> source;
  private final Predicate<? super T> filter;

  public FilteringIterator(Iterator<T> source, Predicate<? super T> filter) {
    this.source = source;
    this.filter = filter;
  }

  @Override
  protected T calcNext() {
    while(source.hasNext()) {
      T value = source.next();
      if(filter.apply(value))
        return value;
    }
    return onStop();
  }
}
