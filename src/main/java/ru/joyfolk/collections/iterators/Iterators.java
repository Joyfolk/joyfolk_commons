package ru.joyfolk.collections.iterators;

import ru.joyfolk.collections.Single;
import ru.joyfolk.functional.Bind;
import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.Function2;
import ru.joyfolk.functional.exceptions.InvocationException;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.functional.predicates.Predicates;
import ru.joyfolk.functional.procedures.Procedure1;
import ru.joyfolk.functional.procedures.simple.Collect;
import ru.joyfolk.functional.simple.Numeric;
import ru.joyfolk.general.Equals;
import ru.joyfolk.reflection.TypeRef;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Iterators {

  public static <R, T> void doWhileTrue(Iterator<? extends T> data, Predicate<? super T> predicate, Function1<R, ? super T> fun) {
    while(data.hasNext()) {
      T val = data.next();
      if(!predicate.apply(val))
        break;
      fun.apply(val);
    }
  }

  public static <T> T findValue(Iterator<? extends T> iter, Predicate<? super T> pred) {
    while(iter.hasNext()) {
      T r = iter.next();
      if(pred.apply(r))
        return r;
    }
    return null;
  }

  public static <I extends Iterator<T>, T> I findFirstAfter(I iter, Predicate<? super T> predicate) {
    while(iter.hasNext()) {
      T val = iter.next();
      if(predicate.apply(val))
        break;
    }
    return iter;
  }

  public static <I extends ListIterator<T>, T> I findFirst(I iter, Predicate<? super T> predicate) {
    while(iter.hasNext()) {
      T val = iter.next();
      if(predicate.apply(val)) {
        iter.previous();
        break;
      }
    }
    return iter;
  }

  public static <T> void foreach(Iterator<? extends T> iter, Function1<?, ? super T> fun) {
    while(iter.hasNext())
      fun.apply(iter.next());
  }

  public static <T> void removeIf(Iterator<? extends T> iter, Predicate<? super T> predicate) {
    while(iter.hasNext()) {
      T val = iter.next();
      if(predicate.apply(val))
        iter.remove();
    }
  }

  public static <T> void retainIf(Iterator<? extends T> iter, Predicate<? super T> predicate) {
    removeIf(iter, Predicates.not(predicate));
  }

  public static <R, T> void map(Iterator<? extends T> iter, final Collection<? super R> target, final Function1<? extends R, ? super T> fun) {
    foreach(iter, new Procedure1<T>() {
      @Override
      protected void execute(T arg) {
        target.add(fun.apply(arg));
      }
    });
  }

  public static <R, T, C extends Collection<R>> C map(Iterator<? extends T> iter, TypeRef<C> targetClss, final Function1<? extends R, ? super T> fun) {
    try {
      C val = targetClss.newInstance();
      map(iter, val, fun);
      return val;
    } catch (InvocationTargetException e) {
      throw new InvocationException(e);
    } catch (NoSuchMethodException e) {
      throw new InvocationException(e);
    } catch (InstantiationException e) {
      throw new InvocationException(e);
    } catch (IllegalAccessException e) {
      throw new InvocationException(e);
    }
  }


  public static <R, T> List<R> mapToArray(Iterator<? extends T> iter, final Function1<? extends R, ? super T> fun) {
    List<R> ret = new ArrayList<R>();
    map(iter, ret, fun);
    return ret;
  }

  public static <R, T> List<R> mapToList(Iterator<? extends T> iter, final Function1<? extends R, ? super T> fun) {
    List<R> ret = new LinkedList<R>();
    map(iter, ret, fun);
    return ret;
  }

  public static <R, T> R accumulate(Iterator<? extends T> iter, R startValue, final Function2<R, R, T> fun) {
    final Single<R> value = new Single<R>(startValue);
    foreach(iter, new Function1<Void, T>() {
      @Override
      public Void apply(T arg) {
        value.set(fun.apply(value.get(), arg));
        return null;
      }
    });
    return value.get();
  }

  public static <I extends Iterator<T>, T> FilteringIterator<T> applyFilter(final I iter, final Predicate<? super T> predicate) {
    return new FilteringIterator<T>(iter, predicate);
  }

  public static <T extends Iterator<?>> int count(T iter) {
    return accumulate(iter, 0, Bind.bindUp1st(Numeric.getIncInt()));
  }

  public static <T> int count(Iterator<T> iter, Predicate<? super T> stopCondition) {
    return accumulate(new ExtractingIterator<T>(iter, stopCondition), 0, Bind.bindUp1st(Numeric.getIncInt()));
  }

  public static <T> Iterator<T> extract(Iterator<T> iter, Predicate<? super T> stopCondition) {
    return new ExtractingIterator<T>(iter, stopCondition);
  }

  public static <T, C extends Collection<? super T>> C collect(Iterator<T> iter, C collection) {
    foreach(iter, Collect.into(collection));
    return collection;
  }

  public static <T> ArrayList<T> collectToArray(Iterator<T> iter) {
    return collect(iter, new ArrayList<T>());
  }

  public static <T> LinkedList<T> collectToList(Iterator<T> iter) {
    return collect(iter, new LinkedList<T>());
  }

  public static <T> HashSet<T> collectToSet(Iterator<T> iter) {
    return collect(iter, new HashSet<T>());
  }
  public static <T, C extends Collection<? super T>> C collect(Iterator<T> iter, TypeRef<C> targetCollectionClss) {
    try {
      C col = targetCollectionClss.newInstance();
      return collect(iter, col);
    }
    catch (NoSuchMethodException e) {
      throw new InvocationException(e);
    }
    catch (IllegalAccessException e) {
      throw new InvocationException(e);
    }
    catch (InvocationTargetException e) {
      throw new InvocationException(e);
    }
    catch (InstantiationException e) {
      throw new InvocationException(e);
    }
  }

  public static <T> Boolean applyAnd(Iterator<? extends T> iter, Predicate<? super T> predicate) {
    while(iter.hasNext())
      if(!predicate.apply(iter.next()))
        return false;
    return true;
  }

  public static <T> Boolean applyOr(Iterator<? extends T> iter, Predicate<? super T> predicate) {
    while(iter.hasNext())
      if(predicate.apply(iter.next()))
        return true;
    return false;
  }

  public static <T> boolean isEquals(Iterator<? extends T> iter1, final Iterator<? extends T> iter2) {
    boolean iter1equals = applyAnd(iter1, new Predicate<T>() {
      @Override
      public Boolean apply(T arg) {
        return iter2.hasNext() && Equals.isEquals(arg, iter2.next()); 
      }
    });
    return iter1equals && !iter2.hasNext();
  }
  
  public static <U, T extends Comparator<? super U>> int applyComparators(Iterator<? extends T> iter, U o1, U o2) {
    while(iter.hasNext()) {
    	int cmp = iter.next().compare(o1, o2);
      if(cmp != 0)
        return cmp;
    }
    return 0;  	
  }
}
