package ru.joyfolk.collections;

import java.util.Comparator;
import java.util.TreeMap;

public class TreeMultiSet<T> extends AbstractMultiSet<T> {
	public TreeMultiSet() {
	  super(new TreeMap<T, Integer>());
  }
	
	public TreeMultiSet(Comparator<? super T> comparator) {
		super(new TreeMap<T, Integer>(comparator));
	}
}
