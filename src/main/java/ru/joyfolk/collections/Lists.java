package ru.joyfolk.collections;

import java.util.*;

public class Lists {
  private Lists() {}
  
  public static <T extends Comparable<? super T>> ListIterator<T> findFirstLarger(List<T> list, T searchItem) {
    ListIterator<T> iter = list.listIterator(0);
    while(iter.hasNext()) {
      T val = iter.next();
      if(val.compareTo(searchItem) < 0)
        break;
    }
    return iter;
  }

  public static <T extends Comparable<? super T>> void insert(List<T> list, T item) {
    ListIterator<T> iter = findFirstLarger(list, item);
    iter.add(item);
  }

  public static <T> ListIterator<T> findFirstLarger(List<T> list, T searchItem, Comparator<? super T> comparator) {
    ListIterator<T> iter = list.listIterator(0);
    while(iter.hasNext()) {
      T val = iter.next();
      if(comparator.compare(val, searchItem) < 0)
        break;
    }
    return iter;
  }

  public static <T> ListIterator<T> insert(List<T> list, T item, Comparator<? super T> comparator) {
    ListIterator<T> iter = findFirstLarger(list, item, comparator);
    iter.add(item);
    return iter;
  }

  public static <T> ArrayList<T> arrayList(T... values) {
    return new ArrayList<T>(Arrays.asList(values));
  }

  public static <T> LinkedList<T> linkedList(T... values) {
    return new LinkedList<T>(Arrays.asList(values));
  }

}
