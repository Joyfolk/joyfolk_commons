package ru.joyfolk.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class AbstractMapWrapper<K, V> implements Map<K, V> {
	private final Map<K, V> wrapped;
	
	protected AbstractMapWrapper(Map<K, V> wrapped) {
		assert wrapped != null: "Wrapped map must be not null";
		this.wrapped = wrapped;
	}

	protected Map<K, V> getWrapped() {
		return wrapped;
	}
	
	@Override
	public void clear() {
		wrapped.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return wrapped.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return wrapped.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return wrapped.entrySet();
	}

	@Override
	public V get(Object key) {
		return wrapped.get(key);
	}

	@Override
	public boolean isEmpty() {
		return wrapped.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return wrapped.keySet();
	}

	@Override
	public V put(K key, V value) {
		return wrapped.put(key, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map) {
		wrapped.putAll(map);
	}

	@Override
	public V remove(Object key) {
		return wrapped.remove(key);
	}

	@Override
	public int size() {
		return wrapped.size();
	}

	@Override
	public Collection<V> values() {
		return wrapped.values();
	}

}
