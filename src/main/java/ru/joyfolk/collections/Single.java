package ru.joyfolk.collections;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;


public class Single<T> extends AbstractCollection<T> implements Set<T> {
  private T value;

  public Single() {
    this(null);
  }

  public Single(T value) {
    set(value);
  }

  public T get() {
    return value;
  }

  public void set(T value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Single(" + value + ")";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(Single.class, value);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Single
        && Equals.isEquals(value, ((Single) o).get());
  }

  @Override
  public boolean add(T value) {
    if(isEmpty()) {
      set(value);
      return true;
    }
    else if(contains(value))
      return false;
    else
      throw new IllegalStateException();
  }

  @Override
  public Iterator<T> iterator() {
    return new Iterator<T>() {
      boolean isFirst = true;

      public boolean hasNext() {
        return isFirst && value != null;
      }

      public T next() {
        if(hasNext()) {
          isFirst = false;
          return value;
        }
        else
          throw new NoSuchElementException();
      }

      public void remove() {
        if(!isFirst)
          value = null;
        else
          throw new IllegalStateException();
      }
    };
  }

  @Override
  public int size() {
    return value == null? 0: 1;
  }
}
