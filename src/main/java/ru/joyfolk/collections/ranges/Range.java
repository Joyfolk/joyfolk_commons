package ru.joyfolk.collections.ranges;

import ru.joyfolk.collections.iterators.AbstractIterator;
import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class Range<T> implements Iterable<T> {
  private final Predicate<? super T> stopCondition;
  private final T startValue;
  private final Function1<? extends T, ? super T> nextValueGenerator;
  private final StopStrategy stopStrategy;

  public Range(T startValue, Predicate<? super T> stopCondition, Function1<? extends T, ? super T> nextValueGenerator) {
    this(startValue, stopCondition, nextValueGenerator, StopStrategy.Immediately);
  }

  public Range(T startValue, Predicate<? super T> stopCondition, Function1<? extends T, ? super T> nextValueGenerator, StopStrategy stopStrategy) {
    this.stopCondition = stopCondition;
    this.startValue = startValue;
    this.nextValueGenerator = nextValueGenerator;
    this.stopStrategy  = stopStrategy;
  }

  final public T getStartValue() {
    return startValue;
  }

  final public Predicate<? super T> getStopCondition() {
    return stopCondition;
  }

  final public Function1<? extends T, ? super T> getNextValueGenerator() {
    return nextValueGenerator;
  }

  @Override
  public String toString() {
    return "Range[" + startValue + ":" + stopCondition + (stopStrategy == StopStrategy.After? "]": ")");
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Range) {
      Range r = (Range) o;
      return Equals.isEquals(startValue, r.startValue)
              && Equals.isEquals(stopStrategy, r.stopStrategy)
              && Equals.isEquals(nextValueGenerator, r.nextValueGenerator)
              && Equals.isEquals(stopStrategy, r.stopStrategy);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(Range.class, startValue, stopStrategy, nextValueGenerator, stopStrategy);
  }

  @Override
  final public RangeIterator iterator() {
    switch (stopStrategy) {
      case Immediately: return new RangeIterator(startValue);
      case After: return new StopAfterConditionRangeIterator(startValue);
    }
    return null;
  }

  enum StopStrategy { Immediately, After }

  protected class RangeIterator extends AbstractIterator<T> {
    T value;

    RangeIterator(T startValue) {
      this.value = startValue;
    }

    @Override
    protected T calcNext() {
      T rValue = value;
      if(stopCondition.apply(rValue))
        return onStop();
      value = nextValueGenerator.apply(value);
      return rValue;
    }
  }

  protected class StopAfterConditionRangeIterator extends RangeIterator {
    boolean stopMe = false;

    StopAfterConditionRangeIterator(T startValue) {
      super(startValue);
    }

    @Override
    protected T calcNext() {
      if(stopMe)
        return onStop();
      T rValue = value;
      if(stopCondition.apply(rValue))
        stopMe = true;
      value = nextValueGenerator.apply(value);
      return rValue;
    }
  }

}
