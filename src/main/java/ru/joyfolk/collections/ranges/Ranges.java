package ru.joyfolk.collections.ranges;

import ru.joyfolk.functional.simple.Numeric;

public class Ranges {
  public static RangeStart<Integer> from(int fromValue) {
    return new RangeStart<Integer>(fromValue, Numeric.getIncInt());
  }

  
}
