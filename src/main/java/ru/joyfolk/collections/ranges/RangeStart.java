package ru.joyfolk.collections.ranges;

import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.functional.predicates.Predicates;
import ru.joyfolk.functional.predicates.simple.IsEquals;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

public class RangeStart<T> {
  final T startValue;
  final Function1<? extends T, ? super T> nextValueGenerator;

  public RangeStart(T startValue, Function1<? extends T, ? super T> nextValueGenerator) {
    assert nextValueGenerator != null: "Генератор следующего значения не должен быть null";
    this.startValue = startValue;
    this.nextValueGenerator = nextValueGenerator;
  }

  public Range<T> whileNot(Predicate<? super T> stopCondition) {
    return new Range<T>(startValue, stopCondition, nextValueGenerator);
  }

  public Range<T> whileTrue(Predicate<? super T> workCondition) {
    return whileNot(Predicates.not(workCondition));
  }

  public Range<T> until(T value) {
    return whileNot(new IsEquals<T>(value));
  }

  public Range<T> whileNotAfter(Predicate<? super T> stopCondition) {
    return new Range<T>(startValue, stopCondition, nextValueGenerator, Range.StopStrategy.After);
  }

  public Range<T> whileAfter(Predicate<? super T> stopCondition) {
    return whileNotAfter(Predicates.not(stopCondition));
  }

  public Range<T> to(T value) {
    return whileNotAfter(new IsEquals<T>(value));
  }

  public String toString() {
    return "RangeStart(" + startValue + ", " + nextValueGenerator + ")";
  }

  public boolean equals(Object o) {
    return o instanceof RangeStart
        && Equals.isEquals(startValue, ((RangeStart) o).startValue)
        && Equals.isEquals(nextValueGenerator, ((RangeStart) o).nextValueGenerator);
  }

  public int hashCode() {
    return HashCode.hashCode(RangeStart.class, startValue, nextValueGenerator);
  }
}
