package ru.joyfolk.collections;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.tuples.Tuple;
import ru.joyfolk.general.tuples.Tuples;
import ru.joyfolk.strings.Strings;

public class TupleSet<T extends Tuple> extends AbstractSet<T> implements Set<T> {
	private final Set<T> content;
	
	public TupleSet() {		
		this(new HashSet<T>());
	}
	
	public TupleSet(Set<T> source) {
		this.content = source;
	}
	
	public TupleSet(Collection<? extends T> tuples) {
		this();
		content.addAll(tuples);
	}
	
	@Override 
	public boolean add(T value) {
		return content.add(value);		
	}

	@Override
	public Iterator<T> iterator() {
		return content.iterator();
	}

	@Override
	public int size() {
		return content.size();
	}
	
	public TupleSet<T> select(final T template) {
		return new TupleSetView<T>(this, new Predicate<T>() {
			@Override
			public String toString() {
				return "Matches(" + template + ")";				
			}
			
			@Override
      public Boolean apply(T arg) {
	      return Tuples.match(template, arg);
      }			
		});
	}
	
	@Override
	public boolean contains(Object o) {
		return content.contains(o);
	}
	
	@Override
	public String toString() {		
		return "TupleSet{" + Strings.join(Strings.COMMA_SEPARATOR, this) + "}";
	}
	
	private static class TupleSetView<T extends Tuple> extends TupleSet<T> {
		final TupleSet<T> source;
		final Predicate<? super T> predicate;
		
		TupleSetView(TupleSet<T> source, Predicate<? super T> predicate) {
			this.source = source;
			this.predicate = predicate;
		}
		
		@Override 
		public boolean add(T value) {
			throw new UnsupportedOperationException("TupleSet view unable to modify parent collection");		
		}

		@Override
		public Iterator<T> iterator() {
			return Iterators.applyFilter(source.iterator(), predicate);
		}

		@Override
		public int size() {
			return Iterators.count(iterator());
		}
		
		@SuppressWarnings("unchecked")
    @Override
		public boolean contains(Object o) {
			return source.contains(o) && predicate.apply((T) o);
		}

		@Override
		public String toString() {
			return "TupleSetView(predicate=" + predicate + ", cont={" + Strings.join(Strings.COMMA_SEPARATOR, this) + "}";
		}
		
	}
}
