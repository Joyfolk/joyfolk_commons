package ru.joyfolk.grammar;

import ru.joyfolk.interfaces.Named;

public enum Case implements Named {
	Nom("Именительный падеж"), Gen("Родительный падеж"), Dat("Дательный падеж"), Acc("Винительный падеж"), Ins("Творительный падеж"), Prep("Предложный падеж");
	
	private final String name;
	
	@Override
	public String toString() { 
		return name;
	}
	
	Case(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}	
}
