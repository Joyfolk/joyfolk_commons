package ru.joyfolk.grammar;

import java.util.Map;

import ru.joyfolk.general.Comparators;

public abstract class AbstractCasedString implements CasedString {
  private final Map<Case, String> map;
  
  protected AbstractCasedString(Map<Case, String> map) {
	 assert map != null && map.containsKey(Case.Nom): "Должен присутствовать хотя бы " + Case.Nom; 
	 this.map = map; 
  }

  @Override
  public String getCaseString(Case grammarCase) {
	if(!map.containsKey(grammarCase))
		return map.get(Case.Nom);
	else
		return map.get(grammarCase);
	
  }
  
  @Override
  public String toString() {
	  return getCaseString(Case.Nom);
  }

  @Override
  public int compareTo(CasedString o) {
  	return Comparators.compareNullsLast(getCaseString(Case.Nom), o.getCaseString(Case.Nom));
  }
}
