package ru.joyfolk.grammar;

public interface CasedString extends Comparable<CasedString> {
	String getCaseString(Case grammarCase);

}
