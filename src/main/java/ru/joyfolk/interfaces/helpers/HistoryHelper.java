package ru.joyfolk.interfaces.helpers;

import java.util.Date;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.predicates.Predicate;
import ru.joyfolk.general.Comparators;
import ru.joyfolk.interfaces.HistoryAttributable;
import ru.joyfolk.interfaces.History;

public class HistoryHelper { 
  public static boolean includesDate(Date date, HistoryAttributable ha) {
	return Comparators.compareNullsFirst(ha.getStartDate(), date) <= 0 
    && Comparators.compareNullsLast(ha.getStopDate(), date) > 0;  
  }
	
  public static <T extends HistoryAttributable> T findForDate(History<T> history, Date date) {
    return Iterators.findValue(history.iterator(), includesDatePredicate(date));	 
  }
  
  public static <T extends HistoryAttributable> Predicate<T> includesDatePredicate(final Date date) {
	return new Predicate<T>() {
		@Override
		public Boolean apply(T arg) {
			return includesDate(date, arg);
		}		
	};
  }
}
