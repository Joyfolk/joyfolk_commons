package ru.joyfolk.interfaces.helpers;

import java.util.Comparator;

import ru.joyfolk.general.Comparators.NullStrategy;
import ru.joyfolk.interfaces.GrammarNamed;
import ru.joyfolk.interfaces.Named;

public class Comparators {
  <T extends Named> Comparator<T> compareName(final NullStrategy strategy) {
	  return new Comparator<T>() {
		@Override
		public int compare(T o1, T o2) {
			if(o1 == o2) 
				return 0;
			else if(o1 == null)
				return strategy.nullValue();
			else if(o2 == null)
				return -strategy.nullValue();
			return ru.joyfolk.general.Comparators.compareNullsLast(o1.getName(), o2.getName());
		}		  
	  };
  }
  
  <T extends GrammarNamed> Comparator<T> compareNameNom(final NullStrategy strategy) {
	  return new Comparator<T>() {
		@Override
		public int compare(T o1, T o2) {
			if(o1 == o2) 
				return 0;
			else if(o1 == null)
				return strategy.nullValue();
			else if(o2 == null)
				return -strategy.nullValue();
			return ru.joyfolk.general.Comparators.compareNullsLast(o1.getName(), o2.getName());
		}		  
	  };
  }
   
}
