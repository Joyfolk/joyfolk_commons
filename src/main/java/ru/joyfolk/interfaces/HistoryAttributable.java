package ru.joyfolk.interfaces;

import java.util.Date;

public interface HistoryAttributable {
	Date getStartDate();
	Date getStopDate();
}
