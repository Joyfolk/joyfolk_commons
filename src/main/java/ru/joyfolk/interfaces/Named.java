package ru.joyfolk.interfaces;

public interface Named {
	String getName();
}
