package ru.joyfolk.interfaces;

import ru.joyfolk.grammar.CasedString;

public interface GrammarNamed {
	CasedString getName();
}
