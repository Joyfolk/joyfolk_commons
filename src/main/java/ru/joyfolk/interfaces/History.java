package ru.joyfolk.interfaces;

import java.util.Date;

public interface History<T extends HistoryAttributable> extends Iterable<T> {
	T getForDate(Date date);
}
