package ru.joyfolk.strings;

import ru.joyfolk.collections.Collections2;
import ru.joyfolk.functional.Function2;

import java.util.Arrays;

public class Strings {
  public static final String EMPTY = "";
  public static final String COMMA_SEPARATOR = ", ";

  public static boolean isEmpty(String str) {
    return str == null || EMPTY.equals(str.trim());
  }


  public static String join(String separator, Object... args) {
    return join(separator, Arrays.asList(args));
  }

  public static String join(String separator, Iterable<?> args) {
    StringBuilder sb = new StringBuilder();
    boolean isFirstArg = true;
    for(Object o: args) {
      if(!isFirstArg)
        sb.append(separator);
      else
        isFirstArg = false;
      sb.append(o);
    }
    return sb.toString();
  }

  public static Boolean contains(String mask, String... samples) {
    assert mask != null: "Маска поиска не должна быть пустой!";
    final String mask0 = mask.trim().toLowerCase();
    return Collections2.accumulate(Arrays.asList(samples), false, new Function2<Boolean, Boolean, String>() {
      @Override
      public Boolean apply(Boolean arg1, String arg2) {
        return arg1 || (arg2 != null && arg2.toLowerCase().contains(mask0));
      }
    });
  }
}
