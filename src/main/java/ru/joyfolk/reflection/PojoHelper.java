package ru.joyfolk.reflection;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class PojoHelper {
	private static final String ID = "id";
	private static final String GETTER_PREFIX = "get";

	public static String doToString(Object pojo) {
		StringBuilder sb = new StringBuilder();
		if(pojo == null)
			sb.append(pojo);
		else {
			sb.append(pojo.getClass().getSimpleName());
			sb.append("[");
			Map<String, Object> fieldValues = getGetterValues(pojo);
			boolean hasPrev = false;
			if(fieldValues.containsKey(ID)) {
				appendKeyValueToStringBuilder(sb, ID, fieldValues.get(ID));
				hasPrev = true;
			}
			for(Map.Entry<String, Object> nameValue: fieldValues.entrySet()) {
				if(hasPrev) 
					sb.append(", ");
				if(!ID.equals(nameValue.getKey()))
					appendKeyValueToStringBuilder(sb, nameValue.getKey(), nameValue.getValue());
				hasPrev = true;
			}
			sb.append("]");			
		}
		return sb.toString();	
	}
	
	public static int hashCode(Object pojo) {
		return getGetterValues(pojo).values().hashCode(); // не эффективно, конечно, в критичных местах не использовать  
	}
		
	public static boolean isEquals(Object pojo1, Object pojo2) {
		if(pojo1 == pojo2)
			return true;
		if(pojo1 == null || pojo2 == null)
			return false;
		if(pojo1.getClass() != pojo2.getClass())
			return false;
		return getGetterValues(pojo1).equals(getGetterValues(pojo2));
	}
	
	private static void appendKeyValueToStringBuilder(StringBuilder sb, String key, Object value) {
		sb.append(key);
		sb.append("=");
		sb.append(value);
	}
	
	private static Map<String, Object> getGetterValues(Object pojo) {
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		if(pojo != null) {
			for(Method method: pojo.getClass().getMethods()) {
				if(method.getParameterTypes().length == 0) {
					String methodName = method.getName();
					if(methodName.startsWith(GETTER_PREFIX) && !GETTER_PREFIX.equals(methodName)) {
						String name = methodName.substring(3, 3).toLowerCase() + methodName.substring(4);
						try {
							fieldValues.put(name, method.invoke(pojo));
						} 
						catch (Exception ignored) { }
					}
				}
			}
		}
		return fieldValues;
	}
}
