package ru.joyfolk.reflection;

import ru.joyfolk.functional.exceptions.InvocationException;
import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * References a generic type.
 * На основе класса TypeReference от crazybob@google.com (Bob Lee)
 */
public abstract class TypeRef<T> {

  private final Type type;
  private volatile Constructor<?> constructor;

  protected TypeRef() {
    Type superclass = getClass().getGenericSuperclass();
    if (superclass instanceof Class) {
      throw new InvocationException("Missing type parameter.");
    }
    this.type = ((ParameterizedType) superclass).getActualTypeArguments()[0];
  }

  public T cast(Object o) {
    Class<?> rawType = type instanceof Class<?>
            ? (Class<?>) type
            : (Class<?>) ((ParameterizedType) type).getRawType();
    return (T) rawType.cast(o);
  }

  @SuppressWarnings("unchecked")
  public T newInstance()
          throws NoSuchMethodException, IllegalAccessException,
          InvocationTargetException, InstantiationException {
    if (constructor == null) {
      Class<?> rawType = type instanceof Class<?>
              ? (Class<?>) type
              : (Class<?>) ((ParameterizedType) type).getRawType();
      constructor = rawType.getConstructor();
    }
    return (T) constructor.newInstance();
  }

  public Type getType() {
    return this.type;
  }

  @Override
  public String toString() {
    return "TypeRef<" + type + ">";
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(TypeRef.class, type, constructor);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof TypeRef
        && Equals.isEquals(type, ((TypeRef) o).type)
        && Equals.isEquals(constructor, ((TypeRef) o).constructor);    
  }
}