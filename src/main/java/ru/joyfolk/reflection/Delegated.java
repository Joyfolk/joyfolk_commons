package ru.joyfolk.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Delegated {
  Class delegateClass();
  String delegateName() default "delegate";
//  boolean override() default false;
}
