package ru.joyfolk.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class PojoProxy {
  public static <T> T createNew(Class<T> clss) {
    return (T) Proxy.newProxyInstance(clss.getClassLoader(), new Class[] { clss }, new PojoHandler());
  }

  private static class PojoHandler implements InvocationHandler {
    private final Map<String, Object> values = new HashMap<String, Object>();

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      String methodName = method.getName();
      if(isGetMethod(methodName))
        return values.get(extractFieldName(methodName));
      else if(isSetMethod(methodName))
        values.put(extractFieldName(methodName), args[0]);
      return null;
    }

    private static boolean isGetMethod(String methodName) {
      return methodName.startsWith("get");
    }

    private static boolean isSetMethod(String methodName) {
      return methodName.startsWith("set");
    }

    private static String extractFieldName(String methodName) {
      return methodName.substring(3);
    }
  }
}