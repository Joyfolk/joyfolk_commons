package ru.joyfolk.reflection;

import ru.joyfolk.general.Equals;
import ru.joyfolk.general.HashCode;
import ru.joyfolk.strings.Strings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodSignature  {
  private final String name;
  private final Class<?>[] parTypes;

  public MethodSignature(String name, Class<?>... parTypes)  {
    this.name = name;
    this.parTypes = parTypes;
  }

  public static MethodSignature forMethod(Method method) {
    return new MethodSignature(method.getName(), method.getParameterTypes());
  }

  public Object invoke(Object object, Object... args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Method method = object.getClass().getMethod(name, parTypes);
    return method.invoke(object, args);
  }

  public boolean isPresentInClass(Class<?> clss) {
    try {
      clss.getMethod(name, parTypes);
      return true;
    }
    catch (NoSuchMethodException e) {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return HashCode.hashCode(name, parTypes);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof MethodSignature
        && Equals.isEquals(name, ((MethodSignature) o).name)
        && Equals.isEquals(parTypes, ((MethodSignature) o).parTypes);
  }

  @Override
  public String toString() {
    return "MethodSignature:" + name + "(" + Strings.join(", ", parTypes) + ")";
  }


}
