package ru.joyfolk.reflection;

import ru.joyfolk.general.tuples.Tuple2;
import ru.joyfolk.general.tuples.Tuples;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Delegation {
  public static <T> T processAnnontation(T object) throws IllegalAccessException, InstantiationException {
    final Map<MethodSignature, Tuple2<String, Class>> methodToNamedClass = new HashMap<MethodSignature, Tuple2<String, Class>>();
    final Map<Tuple2<String, Class>, Object> namedClassToDelegate = new HashMap<Tuple2<String, Class>, Object>();

    for(Method method: object.getClass().getMethods()) {
      if(method.isAnnotationPresent(Delegated.class)) {
        Delegated annotation = method.getAnnotation(Delegated.class);
        String name = annotation.delegateName();
        Class clss = annotation.delegateClass();
        MethodSignature methodSignature = MethodSignature.forMethod(method);
        Tuple2<String, Class> namedClass = Tuples.t(name, clss);
        methodToNamedClass.put(methodSignature, namedClass);
        if(!namedClassToDelegate.containsKey(namedClass))
          namedClassToDelegate.put(namedClass, namedClass.second().newInstance());
      }
    }
    InvocationHandler handler = new InvocationHandler() {
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        MethodSignature signature = MethodSignature.forMethod(method);
        if(methodToNamedClass.containsKey(signature)) {
          Object delegate = namedClassToDelegate.get(methodToNamedClass.get(signature));
          signature.invoke(delegate, args);
        }
        return null;
      }
    };
//    return Proxy.newProxyInstance(object.getClass(), object.getClass().getInterfaces(), handler);
    throw new UnsupportedOperationException("Not implemented yet!"); // todo: not implemented
  }
}
