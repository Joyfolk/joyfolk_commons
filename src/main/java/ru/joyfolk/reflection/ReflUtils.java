package ru.joyfolk.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ReflUtils {
	public static Method getMethodWithSameSignature(Class<?> clss, Method m) {
		try {
	    return clss.getMethod(m.getName(), m.getParameterTypes());
    } 
		catch (SecurityException e) {
			return null;
    } 
		catch (NoSuchMethodException e) {
	    return null;
    }
	}

	public static <I, Ext extends I> Ext decorateInterface(final I object, Class<Ext> clss, final Ext wrapper) {
  	InvocationHandler h = new InvocationHandler() {			
  		@Override
  		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
  			Method sourceMethod = getMethodWithSameSignature(object.getClass(), method);
  			Method wrapperMethod = getMethodWithSameSignature(wrapper.getClass(), method);
  			if(sourceMethod != null)
  				return sourceMethod.invoke(object, args);
  			else
  				return wrapperMethod.invoke(wrapper, args);
  		}
  	};
  	return (Ext) Proxy.newProxyInstance(clss.getClassLoader(), new Class[]{ clss }, h);		
  }
}
