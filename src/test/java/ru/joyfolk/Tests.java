package ru.joyfolk;

import static org.junit.Assert.*;

public class Tests {
  public static void assertException(Class<? extends Throwable> t, Runnable r) {
    boolean catched = false;
    try {
      r.run();
    }
    catch(Throwable e) {
      assertTrue(t.isAssignableFrom(e.getClass()));
      catched = true;
    }
    finally {
      assertTrue("Exception " + t.getSimpleName() + " not catched", catched);
    }
  }

}
