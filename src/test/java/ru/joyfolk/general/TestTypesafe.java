package ru.joyfolk.general;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestTypesafe {
  @Test
  public void testTypesafe() {
    Typesafe<Number> ts = new Typesafe<Number>();
    assertTrue(ts.isEmpty());
    assertNull(ts.getUnsafe());
    assertNull(ts.get(Double.class));
    assertNull(ts.getType());
    ts.put(Double.class, 5.7);
    assertFalse(ts.isEmpty());
    assertEquals(5.7, ts.getUnsafe());
    assertEquals(Double.class, ts.getType());
    assertEquals((Double) 5.7, ts.get(Double.class));
    assertNull(ts.get(Integer.class));
    ts.put(Integer.class, 5);
    assertFalse(ts.isEmpty());
    assertEquals(5, ts.getUnsafe());
    assertEquals(Integer.class, ts.getType());
    assertEquals((Integer) 5, ts.get(Integer.class));
    assertNull(ts.get(Double.class));
    assertNotNull(ts.hashCode());
    assertEquals("Typesafe(5)", ts.toString());
    Typesafe ts2 = new Typesafe();
    ts2.put(Number.class, 5);
    assertEquals(ts, ts2);
    ts.clear();
    assertTrue(ts.isEmpty());
    assertNull(ts.getUnsafe());
    assertNull(ts.get(Double.class));
    assertNull(ts.getType());
  }

}
