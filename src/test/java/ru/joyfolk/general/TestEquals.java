package ru.joyfolk.general;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEquals {
  @Test
  public void testIsEqual() {
    assertTrue(Equals.isEquals(null, null));
    assertTrue(Equals.isEquals("123", "123"));
    assertFalse(Equals.isEquals("123", null));
    assertFalse(Equals.isEquals(null, "321"));
    assertTrue(Equals.isEquals(5, 5));
    assertFalse(Equals.isEquals(5, 6));
  }

  @Test
  public void testIsNotEquals() {
    assertFalse(Equals.isNotEquals(5, 5));
    assertTrue(Equals.isNotEquals(5, 6));
  }
}
