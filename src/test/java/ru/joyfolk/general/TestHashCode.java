package ru.joyfolk.general;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class TestHashCode {
  @Test
  public void testHashCode() {
    assertEquals(HashCode.hashCode("555", 5, 5.4, null), HashCode.hashCode("555", 5, 5.4, null));
    assertFalse(HashCode.hashCode(null, 5, -7, null, null, 5.4, null) == 0);
    assertFalse(HashCode.hashCode(null, null, null, null) == 0);
  }
}
