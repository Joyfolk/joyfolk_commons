package ru.joyfolk.general;

import org.junit.Test;
import ru.joyfolk.reflection.TypeRef;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class TestTypesafeGeneric {
  @Test
  public void testTypesafeGeneric() {
    TypesafeGeneric<List<? extends Number>> ts = new TypesafeGeneric<List<? extends Number>>();
    assertTrue(ts.isEmpty());
    assertNull(ts.getUnsafe());
    assertNull(ts.get(new TypeRef<List<Number>>(){}));
    assertNull(ts.getType());
    ts.put(new TypeRef<List<Double>>(){}, Arrays.asList(5.7));
    assertFalse(ts.isEmpty());
    assertEquals(Arrays.asList(5.7), ts.getUnsafe());
    assertEquals(new TypeRef<List<Double>>(){}, ts.getType());
    assertEquals(Arrays.asList(5.7), ts.get(new TypeRef<List<Double>>(){}));
    assertNull(ts.get(new TypeRef<List<Integer>>(){}));
    ts.put(new TypeRef<List<Integer>>(){}, Arrays.asList(5));
    assertFalse(ts.isEmpty());
    assertEquals(Arrays.asList(5), ts.getUnsafe());
    assertEquals(new TypeRef<List<Integer>>(){}, ts.getType());
    assertEquals(Arrays.asList(5), ts.get(new TypeRef<List<Integer>>(){}));
    assertNull(ts.get(new TypeRef<List<Double>>(){}));
    assertNotNull(ts.hashCode());
    assertTrue(ts.toString().startsWith("TypesafeGeneric"));
    TypesafeGeneric ts2 = new TypesafeGeneric();
    ts2.put(new TypeRef<List>() {}, Arrays.asList(5));
    assertEquals(ts, ts2);
    ts.clear();
    assertTrue(ts.isEmpty());
    assertNull(ts.getUnsafe());
    assertNull(ts.get(new TypeRef<List<Double>>(){}));
    assertNull(ts.get(new TypeRef<List<Integer>>(){}));
    assertNull(ts.getType());
  }

}
