package ru.joyfolk.general;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestComparators {
  @Test
  public void testCompare() {
    assertEquals(0, Comparators.<Integer>compare(null, null, Comparators.NullStrategy.NULL_FIRST));
    assertTrue(Comparators.compare(3, 5, Comparators.NullStrategy.NULL_FIRST) < 0);
    assertEquals(0, Comparators.compare(3, 3, Comparators.NullStrategy.NULL_FIRST));
    assertFalse(Comparators.compare(5, 3, Comparators.NullStrategy.NULL_FIRST) < 0);
    assertEquals(-1, Comparators.compare(null, 3, Comparators.NullStrategy.NULL_FIRST));
    assertEquals(-1, Comparators.compare(3, null, Comparators.NullStrategy.NULL_LAST));
    assertEquals(1, Comparators.compare(null, 3, Comparators.NullStrategy.NULL_LAST));
    assertEquals(1, Comparators.compare(3, null, Comparators.NullStrategy.NULL_FIRST));
  }

  @Test
  public void testCompareNullsLast() {
    assertTrue(Comparators.compareNullsLast(null, 3) > 0);
    assertTrue(Comparators.compareNullsLast(3, null) < 0);
  }

  @Test
  public void testCompareNullsFirst() {
    assertTrue(Comparators.compareNullsFirst(null, 3) < 0);
    assertTrue(Comparators.compareNullsFirst(3, null) > 0);
  }

  @Test
  public void testCompareWithComparator() {
    assertEquals(0, Comparators.compare(null, null, Comparators.NullStrategy.NULL_FIRST, Comparators.TOSTRING));
    assertTrue(Comparators.compare(3, 27, Comparators.NullStrategy.NULL_LAST, Comparators.TOSTRING) > 0);
    assertTrue(Comparators.compare(27, 3, Comparators.NullStrategy.NULL_LAST, Comparators.TOSTRING) < 0);
    assertEquals(0, Comparators.compare(27, 27, Comparators.NullStrategy.NULL_LAST, Comparators.TOSTRING));
    assertEquals(-1, Comparators.compare(null, 3, Comparators.NullStrategy.NULL_FIRST, Comparators.TOSTRING));
    assertEquals(-1, Comparators.compare(3, null, Comparators.NullStrategy.NULL_LAST, Comparators.TOSTRING));
    assertEquals(1, Comparators.compare(null, 3, Comparators.NullStrategy.NULL_LAST, Comparators.TOSTRING));
    assertEquals(1, Comparators.compare(3, null, Comparators.NullStrategy.NULL_FIRST, Comparators.TOSTRING));
  }

	@Test
	public void testChain() {
		fail("Not yet implemented");
	}

	@Test
	public void testCompareAll() {
		fail("Not yet implemented");
	}


}
