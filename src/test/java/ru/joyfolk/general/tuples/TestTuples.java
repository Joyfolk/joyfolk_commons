package ru.joyfolk.general.tuples;

import static org.junit.Assert.*;
import static ru.joyfolk.general.tuples.Tuples.t;

import org.junit.Test;

import ru.joyfolk.general.Equals;

public class TestTuples {
	static class A {
		final int value;
		
		A(int value) {
			this.value = value;
		}
		
		@Override
		public boolean equals(Object oth) {
			return oth instanceof A 
			    && Equals.isEquals(value, ((A) oth).value);
		}
	}

	static class B extends A {
		B(int value) {
	    super(value);
    } 
	};
    
	
	@Test
	public void testMatch() {
		assertTrue(Tuples.match(new Tuple1<A>(null), Tuples.t(new A(0))));
		assertTrue(Tuples.match(new Tuple1<A>(new A(1)), Tuples.t(new A(1))));
		assertFalse(Tuples.match(new Tuple1<A>(new A(2)), Tuples.t(new A(1))));
		assertTrue(Tuples.match(new Tuple1<A>(new A(1)), Tuples.t(new B(1))));
		assertTrue(Tuples.match(new Tuple1<A>(null), new Tuple1<A>(null)));
		assertFalse(Tuples.match(new Tuple1<A>(new A(1)), new Tuple1<A>(null)));
		
		assertTrue(Tuples.match(new Tuple2<A, A>(null, null), Tuples.t(new A(0), new B(0))));
		assertTrue(Tuples.match(new Tuple2<A, A>(new A(1), null), Tuples.t(new A(1), new B(0))));
		assertFalse(Tuples.match(new Tuple2<A, A>(new A(2), null), Tuples.t(new A(1), new B(0))));
		assertTrue(Tuples.match(new Tuple2<A, A>(new A(1), null), Tuples.t(new B(1), new B(0))));
		assertTrue(Tuples.match(new Tuple2<A, A>(null, null), new Tuple2<A, B>(null, null)));
		assertFalse(Tuples.match(new Tuple2<A, A>(new A(1), null), new Tuple2<A, B>(null, new B(0))));
		
		assertTrue(Tuples.match(new Tuple3<A, A, A>(null, null, new B(5)), Tuples.t(new A(0), new B(0), new B(5))));
		assertTrue(Tuples.match(new Tuple3<A, A, A>(new A(1), null, null), Tuples.t(new A(1), new B(0), new B(5))));
		assertFalse(Tuples.match(new Tuple3<A, A, A>(new A(2), null, null), Tuples.t(new A(1), new B(0), new B(5))));
		assertTrue(Tuples.match(new Tuple3<A, A, A>(new A(1), null, null), Tuples.t(new B(1), new B(0), new B(5))));
		assertTrue(Tuples.match(new Tuple3<A, A, A>(null, null, null), new Tuple3<A, B, B>(null, null, new B(5))));
		assertFalse(Tuples.match(new Tuple3<A, A, A>(new A(1), null, null), new Tuple3<A, B, B>(null, new B(0), new B(5))));
		
		assertTrue(Tuples.match(new Tuple4<A, A, A, A>(null, null, new B(5), null), Tuples.t(new A(0), new B(0), new B(5), (A) null)));
		assertTrue(Tuples.match(new Tuple4<A, A, A, A>(new A(1), null, null, null), Tuples.t(new A(1), new B(0), new B(5), (A) null)));
		assertFalse(Tuples.match(new Tuple4<A, A, A, A>(new A(2), null, null, null), Tuples.t(new A(1), new B(0), new B(5), new A(6))));
		assertTrue(Tuples.match(new Tuple4<A, A, A, A>(new A(1), null, null, null), Tuples.t(new B(1), new B(0), new B(5), new A(6))));
		assertTrue(Tuples.match(new Tuple4<A, A, A, A>(null, null, null, null), new Tuple4<A, B, B, A>(null, null, new B(5), new A(6))));
		assertFalse(Tuples.match(new Tuple4<A, A, A, A>(new A(1), null, null, null), new Tuple4<A, B, B, A>(null, new B(0), new B(5), new A(6))));
		
		assertTrue(Tuples.match(new Tuple5<A, A, B, A, A>(null, null, new B(5), null, null), Tuples.t(new A(0), new B(0), new B(5), (A) null,(A)  null)));
		assertTrue(Tuples.match(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), Tuples.t(new A(1), new B(0), new B(5), (A) null, (A) null)));
		assertFalse(Tuples.match(new Tuple5<A, A, A, A, A>(new A(2), null, null, null, null), Tuples.t(new A(1), new B(0), new B(5), new A(6), (A) null)));
		assertTrue(Tuples.match(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), Tuples.t(new B(1), new B(0), new B(5), new A(6), (A) null)));
		assertTrue(Tuples.match(new Tuple5<A, A, A, A, A>(null, null, null, null, null), new Tuple5<A, B, B, A, A>(null, null, new B(5), new A(6), null)));
		assertFalse(Tuples.match(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), new Tuple5<A, B, B, A, A>(null, new B(0), new B(5), new A(6), null)));
		
		assertFalse(Tuples.match(t((Integer) null, "2", "t2"), t(3, "1", "t1")));		
		assertTrue(Tuples.match(t((Integer) null, "2", "t2"), t(3, "2", "t2")));
		assertFalse(Tuples.match(t((Integer) null, "2", "t2"), t(3, "3", "t3")));				
	}
	
	@Test
	public void testMatch1() {
		assertTrue(Tuples.match1(new Tuple1<A>(null), Tuples.t(new A(0))));
		assertTrue(Tuples.match1(new Tuple1<A>(new A(1)), Tuples.t(new A(1))));
		assertFalse(Tuples.match1(new Tuple1<A>(new A(2)), Tuples.t(new A(1))));
		assertTrue(Tuples.match1(new Tuple1<A>(new A(1)), Tuples.t(new B(1))));
		assertTrue(Tuples.match1(new Tuple1<A>(null), new Tuple1<A>(null)));
		assertFalse(Tuples.match1(new Tuple1<A>(new A(1)), new Tuple1<A>(null)));
	}

	@Test
	public void testMatch2() {
		assertTrue(Tuples.match2(new Tuple2<A, A>(null, null), Tuples.t(new A(0), new B(0))));
		assertTrue(Tuples.match2(new Tuple2<A, A>(new A(1), null), Tuples.t(new A(1), new B(0))));
		assertFalse(Tuples.match2(new Tuple2<A, A>(new A(2), null), Tuples.t(new A(1), new B(0))));
		assertTrue(Tuples.match2(new Tuple2<A, A>(new A(1), null), Tuples.t(new B(1), new B(0))));
		assertTrue(Tuples.match2(new Tuple2<A, A>(null, null), new Tuple2<A, B>(null, null)));
		assertFalse(Tuples.match2(new Tuple2<A, A>(new A(1), null), new Tuple2<A, B>(null, new B(0))));
	}

	@Test
	public void testMatch3() {
		assertTrue(Tuples.match3(new Tuple3<A, A, A>(null, null, new B(5)), Tuples.t(new A(0), new B(0), new B(5))));
		assertTrue(Tuples.match3(new Tuple3<A, A, A>(new A(1), null, null), Tuples.t(new A(1), new B(0), new B(5))));
		assertFalse(Tuples.match3(new Tuple3<A, A, A>(new A(2), null, null), Tuples.t(new A(1), new B(0), new B(5))));
		assertTrue(Tuples.match3(new Tuple3<A, A, A>(new A(1), null, null), Tuples.t(new B(1), new B(0), new B(5))));
		assertTrue(Tuples.match3(new Tuple3<A, A, A>(null, null, null), new Tuple3<A, B, B>(null, null, new B(5))));
		assertFalse(Tuples.match3(new Tuple3<A, A, A>(new A(1), null, null), new Tuple3<A, B, B>(null, new B(0), new B(5))));
		
		assertFalse(Tuples.match3(t((Integer) null, "2", "t2"), t(3, "1", "t1")));		
		assertTrue(Tuples.match3(t((Integer) null, "2", "t2"), t(3, "2", "t2")));
		assertFalse(Tuples.match3(t((Integer) null, "2", "t2"), t(3, "3", "t3")));		
	}

	@Test
	public void testMatch4() {
		assertTrue(Tuples.match4(new Tuple4<A, A, A, A>(null, null, new B(5), null), Tuples.t(new A(0), new B(0), new B(5), (A) null)));
		assertTrue(Tuples.match4(new Tuple4<A, A, A, A>(new A(1), null, null, null), Tuples.t(new A(1), new B(0), new B(5), (A) null)));
		assertFalse(Tuples.match4(new Tuple4<A, A, A, A>(new A(2), null, null, null), Tuples.t(new A(1), new B(0), new B(5), new A(6))));
		assertTrue(Tuples.match4(new Tuple4<A, A, A, A>(new A(1), null, null, null), Tuples.t(new B(1), new B(0), new B(5), new A(6))));
		assertTrue(Tuples.match4(new Tuple4<A, A, A, A>(null, null, null, null), new Tuple4<A, B, B, A>(null, null, new B(5), new A(6))));
		assertFalse(Tuples.match4(new Tuple4<A, A, A, A>(new A(1), null, null, null), new Tuple4<A, B, B, A>(null, new B(0), new B(5), new A(6))));
	}

	@Test
	public void testMatch5() {
		assertTrue(Tuples.match5(new Tuple5<A, A, B, A, A>(null, null, new B(5), null, null), Tuples.t(new A(0), new B(0), new B(5), (A) null,(A)  null)));
		assertTrue(Tuples.match5(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), Tuples.t(new A(1), new B(0), new B(5), (A) null, (A) null)));
		assertFalse(Tuples.match5(new Tuple5<A, A, A, A, A>(new A(2), null, null, null, null), Tuples.t(new A(1), new B(0), new B(5), new A(6), (A) null)));
		assertTrue(Tuples.match5(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), Tuples.t(new B(1), new B(0), new B(5), new A(6), (A) null)));
		assertTrue(Tuples.match5(new Tuple5<A, A, A, A, A>(null, null, null, null, null), new Tuple5<A, B, B, A, A>(null, null, new B(5), new A(6), null)));
		assertFalse(Tuples.match5(new Tuple5<A, A, A, A, A>(new A(1), null, null, null, null), new Tuple5<A, B, B, A, A>(null, new B(0), new B(5), new A(6), null)));		
	}
	
	@Test
	public void testFirstFifth() {
		assertEquals(1, Tuples.first().apply(Tuples.t(1, 2, 3, 4, 5)));
		assertEquals(2, Tuples.second().apply(Tuples.t(1, 2, 3, 4, 5)));
		assertEquals(3, Tuples.third().apply(Tuples.t(1, 2, 3, 4, 5)));
		assertEquals(4, Tuples.fourth().apply(Tuples.t(1, 2, 3, 4, 5)));
		assertEquals(5, Tuples.fifth().apply(Tuples.t(1, 2, 3, 4, 5)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetNth() {
		assertEquals(1, Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 0));
		assertEquals(2, Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 1));
		assertEquals(3, Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 2));
		assertEquals(4, Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 3));
		assertEquals(5, Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 4));
		Tuples.getNth(Tuples.t(1, 2, 3, 4, 5), 6);
	}
	
	@Test
	public void testAsList() {	
		fail("not implemented yet");
	}
	
	@Test
	public void testAsArray() {
		fail("not implemented yet");		
	}
}