package ru.joyfolk.collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class TestLists {
  @Test
  public void testArrayList() {
    ArrayList<Integer> itgr = Lists.arrayList(1, 2, 3, 4, 5);
    assertEquals(Arrays.asList(1, 2, 3, 4, 5), itgr);
    assertEquals(ArrayList.class, itgr.getClass());
  }

  @Test
  public void testLinkedList() {
    LinkedList<Integer> itgr = Lists.linkedList(1, 2, 3, 4, 5);
    assertEquals(Arrays.asList(1, 2, 3, 4, 5), itgr);
    assertEquals(LinkedList.class, itgr.getClass());
  }
  
  @Test
  public void testNil() {
//    final Seq<Integer> seq = Seq.nil();
//    assertEquals(seq, Seq.<Integer>nil());
//    assertEquals(0, seq.size());
//    assertTrue(seq.isEmpty());
//    assertTrue(seq.isNil());
//    assertTrue(seq.isLastElem());
//    assertException(NoSuchElementException.class, new Runnable() { public void run() { seq.head(); } });
//    assertException(NoSuchElementException.class, new Runnable() { public void run() { seq.tail(); } });
//    final SeqIterator<Integer> iter = seq.iterator();
//    assertNotNull(iter);
//    assertFalse(iter.hasNext());
//    assertException(NoSuchElementException.class, new Runnable() { public void run() { iter.next(); } });
//    assertException(UnsupportedOperationException.class, new Runnable() { public void run() { iter.remove(); } });
  }

}
