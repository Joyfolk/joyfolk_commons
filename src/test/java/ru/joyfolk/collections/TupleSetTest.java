package ru.joyfolk.collections;

import static org.junit.Assert.*;
import static ru.joyfolk.general.tuples.Tuples.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.general.Comparators;
import ru.joyfolk.general.tuples.Tuple3;

public class TupleSetTest {

	@Test
	public void testSize() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>();
		ts.addAll(Arrays.asList(t(3, "3", "t3"), t(3, "3", "t3"), t(4, "5", "t6")));
		assertEquals(2, ts.size());
	}

	@Test
	public void testTupleSet() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>();
		assertTrue(ts.isEmpty());
	}

	@Test
	public void testTupleSetSet() {
		Comparator<Tuple3<Integer, String, String>> comp = new Comparator<Tuple3<Integer, String, String>>() {
			@Override
      public int compare(Tuple3<Integer, String, String> o1, Tuple3<Integer, String, String> o2) {
	      return Comparators.chain()
	      						.compareNullsFirst(o1.first(), o2.first())
	      						.compareNullsLast(o1.second(), o2.second())
	      						.compareNullsFirst(o1.third(), o2.third())
	      			 .result();
      }			
		};
		
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(
				new TreeSet<Tuple3<Integer, String, String>>(
				)
		);
		assertTrue(ts.isEmpty());
	}	
	
	@Test
	public void testTupleSetCollection() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(Arrays.asList(t(3, "3", "t3"), t(3, "3", "t3"), t(4, "5", "t6")));
		assertEquals(2, ts.size());
	}

	@Test
	public void testAdd() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(Arrays.asList(t(3, "3", "t3"), t(3, "3", "t3"), t(4, "5", "t6")));
		ts.add(t(5, "5", "t5"));
		assertEquals(3, ts.size());
		assertTrue(ts.contains(t(5, "5", "t5")));
	}
	
	@Test
	public void testContains() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(Arrays.asList(t(3, "3", "t3"), t(3, "3", "t3"), t(4, "5", "t6")));
		assertFalse(ts.contains(t(5, "5", "t5")));
		assertTrue(ts.contains(t(4, "5", "t6")));
		assertTrue(ts.contains(t(3, "3", "t3")));
	}
	

	@Test
	public void testIterator() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(Arrays.asList(t(3, "3", "t3"), t(3, "3", "t3"), t(4, "5", "t6")));
		Set<Tuple3<Integer, String, String>> cs = Iterators.collectToSet(ts.iterator());
		assertTrue(cs.contains(t(3, "3", "t3")));
		assertTrue(cs.contains(t(4, "5", "t6")));
		assertEquals(2, cs.size());
		ts.remove(t(3, "3", "t3"));
		assertEquals(1, ts.size());
	}

	@Test
	public void testSelect() {
		TupleSet<Tuple3<Integer, String, String>> ts = new TupleSet<Tuple3<Integer,String,String>>(Arrays.asList(
				t(3, "1", "t1"), t(4, "2", "t1"), t(5, "5", "t3"), t(6, "2", "t4"), t(6, "6", "t5"),
				t(3, "2", "t2"), t(4, "3", "t2"), t(5, "5", "t4"), t(6, "3", "t3"), t(5, "5", "t6"),
				t(3, "3", "t3"), t(4, "4", "t4"), t(5, "5", "t5"), t(6, "6", "t6"), t(7, "7", "t7") 
		));
		TupleSet<Tuple3<Integer, String, String>> view = ts.select(t(3, (String) null, (String) null));
		assertEquals(3, view.size());
		assertTrue(view.contains(t(3, "1", "t1")));
		assertTrue(view.contains(t(3, "2", "t2")));
		assertTrue(view.contains(t(3, "3", "t3")));
		Set<Tuple3<Integer, String, String>> cs = Iterators.collectToSet(view.iterator());
		assertTrue(cs.contains(t(3, "1", "t1")));
		assertTrue(cs.contains(t(3, "2", "t2")));
		assertTrue(cs.contains(t(3, "3", "t3")));
		assertEquals(3, cs.size());
		TupleSet<Tuple3<Integer, String, String>> view2 = view.select(t((Integer) null, "2", "t2"));
		assertEquals(1, view2.size());
		assertTrue(view2.contains(t(3, "2", "t2")));
		TupleSet<Tuple3<Integer, String, String>> view3 = view2.select(t((Integer) null, "3", "t2"));
		assertTrue(view3.isEmpty());		
	}


}
