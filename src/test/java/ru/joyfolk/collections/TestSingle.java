package ru.joyfolk.collections;

import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.junit.Assert.*;

public class TestSingle {
  @Test
  public void testGetSet() {
    Single<Integer> s = new Single<Integer>();
    assertNull(s.get());
    assertEquals(0, s.size());
    s.set(5);
    assertEquals(1, s.size());
    assertEquals(Integer.valueOf(5), s.get());
    s.set(7);
    assertEquals(Integer.valueOf(7), s.get());
    s.set(null);
    assertEquals(0, s.size());
    assertNull(s.get());
  }

  @Test(expected = IllegalStateException.class)
  public void testSetInterface() {
    Set<Integer> s = new Single<Integer>();
    assertTrue(s.isEmpty());
    assertTrue(s.add(5));
    assertEquals(1, s.size());
    assertFalse(s.add(5));
    assertEquals(1, s.size());
    s.add(7);
  }

  @Test
  public void testSetInterface2() {
    Set<Integer> s = new Single<Integer>();
    s.add(5);
    assertTrue(s.contains(5));
    s.remove(5);
    assertFalse(s.contains(5));
    assertEquals(0, s.size());
    s.remove(7);
    assertFalse(s.contains(5));
    assertEquals(0, s.size());    
  }

  @Test(expected = IllegalStateException.class)
  public void testIterator() {
    Set<Integer> s = new Single<Integer>(5);
    Iterator<Integer> i = s.iterator();
    assertTrue(i.hasNext());
    i.remove();
  }

  @Test(expected = NoSuchElementException.class)
  public void testIterator2() {
    Set<Integer> s = new Single<Integer>(5);
    Iterator<Integer> i = s.iterator();
    assertEquals(Integer.valueOf(5), i.next());
    assertFalse(i.hasNext());
    i.remove();
    assertTrue(s.isEmpty());
    s.add(7);
    i = s.iterator();
    i.next();
    i.next();
  }

}
