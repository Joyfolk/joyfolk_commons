package ru.joyfolk.collections.iterators;

import org.junit.Test;
import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.predicates.Predicates;
import ru.joyfolk.functional.predicates.simple.IsEquals;
import ru.joyfolk.functional.predicates.simple.IsLarger;
import ru.joyfolk.functional.predicates.simple.IsLesser;
import ru.joyfolk.functional.predicates.simple.NumberPredicates;
import ru.joyfolk.functional.procedures.simple.Collect;
import ru.joyfolk.functional.simple.Numeric;
import ru.joyfolk.reflection.TypeRef;

import java.util.*;

import static org.junit.Assert.*;

public class TestIterators {
  @Test
  public void testDoWhileTrue() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5);
    {
      final StringBuilder sb = new StringBuilder();
      Iterators.doWhileTrue(integers.iterator(), IsLesser.than(5), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          sb.append(arg);
          return null;
        }
      });
    }
    {
      final StringBuilder sb = new StringBuilder();
      Iterators.doWhileTrue(integers.iterator(), IsLarger.than(0), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          sb.append(arg);
          return null;
        }
      });
      assertEquals("12345", sb.toString());
    }
    {
      final StringBuilder sb = new StringBuilder();
      Iterators.doWhileTrue(integers.iterator(), IsLesser.than(0), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          sb.append(arg);
          return null;
        }
      });
      assertEquals("", sb.toString());
    }
    {
      final StringBuilder sb = new StringBuilder();
      Iterators.doWhileTrue(Collections.<Integer>emptyList().iterator(), IsLesser.than(0), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          sb.append(arg);
          return null;
        }
      });
      assertEquals("", sb.toString());
    }
  }

  @Test
  public void testFindValue() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 6);
    assertEquals(Integer.valueOf(6), Iterators.findValue(integers.iterator(), NumberPredicates.isLargerOrEquals(5)));
    assertNull(Iterators.findValue(integers.iterator(), NumberPredicates.isLargerOrEquals(7)));
    assertNull(Iterators.findValue(Collections.<Integer>emptyList().iterator(), NumberPredicates.isLargerOrEquals(7)));
  }

  @Test
  public void testFindFirstAfter() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5);
    ListIterator<Integer> iter = Iterators.findFirstAfter(integers.listIterator(), new IsEquals<Integer>(3));
    assertTrue(iter.hasNext());
    assertTrue(iter.hasPrevious());
    assertEquals(Integer.valueOf(4), iter.next());
    assertEquals(Integer.valueOf(5), iter.next());
  }

  @Test
  public void testFindFirst() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5);
    ListIterator<Integer> iter = Iterators.findFirst(integers.listIterator(), new IsEquals<Integer>(3));
    assertTrue(iter.hasNext());
    assertTrue(iter.hasPrevious());
    assertEquals(Integer.valueOf(3), iter.next());
    assertEquals(Integer.valueOf(4), iter.next());
  }

  @Test
  public void testForeach() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = new ArrayList<String>();
      Iterators.foreach(integers.iterator(), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          list.add(arg.toString());
          return null;
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = new ArrayList<String>();
      Iterators.foreach(integers.iterator(), new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          list.add(arg.toString());
          return null;
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMap() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = new ArrayList<String>();
      Iterators.map(integers.iterator(), list, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = new ArrayList<String>();
      Iterators.map(integers.iterator(), list, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMapClss() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      List<String> list = Iterators.map(integers.iterator(), new TypeRef<ArrayList<String>>(){}, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertTrue(list instanceof ArrayList);
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list =  Iterators.map(integers.iterator(), new TypeRef<LinkedList<String>>(){}, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertTrue(list instanceof LinkedList);
      assertEquals(Collections.<String>emptyList(), list);
    }
  }


  @Test
  public void testMapToArray() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = Iterators.mapToArray(integers.iterator(), new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = Iterators.mapToArray(integers.iterator(), new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMapToList() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      List<String> list = Iterators.mapToList(integers.iterator(), new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      List<String> list = Iterators.mapToList(integers.iterator(), new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testAccumulate() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5);
    assertEquals(Integer.valueOf(26), Iterators.accumulate(integers.iterator(), 11, Numeric.getSumInt()));
    assertEquals(Integer.valueOf(240),Iterators.accumulate(integers.iterator(), 2, Numeric.getMultInt()));
    assertEquals(Integer.valueOf(2), Iterators.accumulate(Collections.<Integer>emptyList().iterator(), 2, Numeric.getMultInt()));
  }

  @Test
  public void testApplyFilter() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    List<Integer> evens = new ArrayList<Integer>();
    Iterators.foreach(Iterators.applyFilter(integers.iterator(), NumberPredicates.isEvenInt()), Collect.into(evens));
    assertEquals(Arrays.asList(2, 4, 6), evens);
    evens.clear();
    Iterators.foreach(Iterators.applyFilter(Collections.<Integer>emptyList().iterator(), NumberPredicates.isEvenInt()), Collect.into(evens));
    assertEquals(Collections.<Integer>emptyList(), evens);
  }

  @Test
  public void testCount() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    assertEquals(7, Iterators.count(integers.iterator()));
  }

  @Test
  public void testCount2() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    assertEquals(4, Iterators.count(integers.iterator(), Predicates.isEquals(5)));
  }

  @Test
  public void testExtract() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Iterator<Integer> i = Iterators.extract(integers.iterator(), Predicates.isEquals(7));
      assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6), Iterators.collectToArray(i));
    }
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Iterator<Integer> i = Iterators.extract(integers.iterator(), Predicates.isEquals(11));
      assertEquals(integers, Iterators.collectToArray(i));
    }
    {
      Iterator<Integer> i = Iterators.extract(Collections.<Integer>emptyList().iterator(), Predicates.isEquals(11));
      assertEquals(Collections.<Integer>emptyList(), Iterators.collectToArray(i));
    }
  }

  @Test
  public void testCollect() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = new HashSet<Integer>();
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = new HashSet<Integer>();
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToClss() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = Iterators.collect(integers.iterator(), new TypeRef<HashSet<Integer>>(){});
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = Iterators.collect(integers.iterator(), new TypeRef<HashSet<Integer>>(){});
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToSet() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = Iterators.collectToSet(integers.iterator());
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = Iterators.collectToSet(integers.iterator());
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToList() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      LinkedList<Integer> integers2 = Iterators.collectToList(integers.iterator());
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      LinkedList<Integer> integers2 = Iterators.collectToList(integers.iterator());
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToArray() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      ArrayList<Integer> integers2 = Iterators.collectToArray(integers.iterator());
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      ArrayList<Integer> integers2 = Iterators.collectToArray(integers.iterator());
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testApplyAnd() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      assertTrue(Iterators.applyAnd(integers.iterator(), NumberPredicates.isLarger(0)));
    }
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      assertFalse(Iterators.applyAnd(integers.iterator(), NumberPredicates.isLarger(5)));
    }
    {
      assertTrue(Iterators.applyAnd(Collections.<Integer>emptyList().iterator(), NumberPredicates.isLarger(5)));
    }
  }

  @Test
  public void testApplyOr() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      assertTrue(Iterators.applyOr(integers.iterator(), NumberPredicates.isEvenInt()));
    }
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 3, 5, 7, 9));
      assertFalse(Iterators.applyOr(integers.iterator(), NumberPredicates.isEvenInt()));
    }
    {
      assertFalse(Iterators.applyOr(Collections.<Integer>emptyList().iterator(), NumberPredicates.isLarger(5)));
    }
  }

}
