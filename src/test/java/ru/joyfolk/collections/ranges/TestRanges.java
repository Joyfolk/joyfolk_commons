package ru.joyfolk.collections.ranges;

import org.junit.Test;
import ru.joyfolk.collections.Collections2;
import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.simple.Numeric;
import ru.joyfolk.reflection.TypeRef;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestRanges {
  @Test
  public void test() {
    assertEquals(Integer.valueOf(500500), Collections2.accumulate(Ranges.from(0).to(1000), 0, Numeric.getSumInt()));
    assertEquals(Arrays.asList(1, 2, 3), Iterators.collect(Ranges.from(1).until(4).iterator(), new TypeRef<ArrayList<Integer>>(){}));
    assertEquals(Arrays.asList(1, 2, 3, 4), Iterators.collect(Ranges.from(1).to(4).iterator(), new TypeRef<ArrayList<Integer>>(){}));    
  }
}
