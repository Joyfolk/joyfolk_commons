package ru.joyfolk.collections;

import org.junit.Test;
import ru.joyfolk.collections.iterators.SeqIterator;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;
import static ru.joyfolk.Tests.assertException;

public class TestSeq {
  @Test
  public void testNil() {
    final Seq<Integer> seq = Seq.nil();
    assertEquals(seq, Seq.<Integer>nil());
    assertEquals(0, seq.size());
    assertTrue(seq.isEmpty());
    assertTrue(seq.isNil());
    assertTrue(seq.isLastElem());
    assertException(NoSuchElementException.class, new Runnable() { public void run() { seq.head(); } });
    assertException(NoSuchElementException.class, new Runnable() { public void run() { seq.tail(); } });
    final SeqIterator<Integer> iter = seq.iterator();
    assertNotNull(iter);
    assertFalse(iter.hasNext());
    assertException(NoSuchElementException.class, new Runnable() { public void run() { iter.next(); } });
    assertException(UnsupportedOperationException.class, new Runnable() { public void run() { iter.remove(); } });
  }

  @Test
  public void testFrom() {
    final Seq<Integer> seq = Seq.from(1, 3, 5, 7, 11, 19);
    assertEquals(6, seq.size());
    assertTrue(seq.contains(1));
    assertTrue(seq.contains(3));
    assertTrue(seq.contains(5));
    assertTrue(seq.contains(7));
    assertTrue(seq.contains(11));
    assertTrue(seq.contains(19));
  }

  @Test
  public void testFrom2() {
    final Seq<Integer> seq = Seq.from(Arrays.asList(1, 3, 5, 7, 11, 19).iterator());
    assertEquals(6, seq.size());
    assertTrue(seq.contains(1));
    assertTrue(seq.contains(3));
    assertTrue(seq.contains(5));
    assertTrue(seq.contains(7));
    assertTrue(seq.contains(11));
    assertTrue(seq.contains(19));
  }


  @Test
  public void testAppendAll() {
    Seq<Integer> seq = Seq.from(Arrays.asList(1, 3, 5, 7, 11, 19).iterator());
    seq = seq.appendAll(Seq.from(32, 33, 45));
    assertEquals(9, seq.size());
    assertTrue(seq.contains(32));
    assertTrue(seq.contains(33));
    assertTrue(seq.contains(45));
  }


  @Test
  public void testIterator() {
    Seq<Integer> seq = Seq.from(1, 3, 5);
    final SeqIterator<Integer> iter = seq.iterator();
    assertTrue(iter.hasNext());
    assertEquals(Integer.valueOf(1), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(Integer.valueOf(3), iter.next());
    assertTrue(iter.hasNext());
    assertEquals(Integer.valueOf(5), iter.next());
    assertFalse(iter.hasNext());
    assertException(NoSuchElementException.class, new Runnable() { public void run() { iter.next(); } });
    assertException(UnsupportedOperationException.class, new Runnable() { public void run() { iter.remove(); } });
  }

  @Test
  public void testOther() {
    Seq<Integer> seq = Seq.nil();
    seq = seq.append(1);
    assertTrue(seq.contains(1));
    assertTrue(seq.isLastElem());
    Seq<Integer> seq2 = seq.append(2);
    assertTrue(seq2.contains(2));
    assertFalse(seq2.isLastElem());
    assertEquals("2:1:Nil", seq2.toString());
    int i1 = seq2.hashCode();
    int i2 = seq2.hashCode();
    int i3 = seq.hashCode();
    assertTrue(i1 == i2);
    assertFalse(i3 == i2);
  }

}
