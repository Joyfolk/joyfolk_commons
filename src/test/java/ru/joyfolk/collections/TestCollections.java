package ru.joyfolk.collections;

import org.junit.Test;
import ru.joyfolk.collections.iterators.Iterators;
import ru.joyfolk.functional.Function0;
import ru.joyfolk.functional.Function1;
import ru.joyfolk.functional.predicates.simple.NumberPredicates;
import ru.joyfolk.functional.procedures.simple.Collect;
import ru.joyfolk.functional.simple.Numeric;
import ru.joyfolk.general.Comparators;
import ru.joyfolk.general.tuples.Tuple1;
import ru.joyfolk.general.tuples.Tuples;
import ru.joyfolk.reflection.TypeRef;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestCollections {
  @Test
  public void testRemoveRetainIf() {
    Collections2.removeIf(Collections.<Integer>emptyList(), NumberPredicates.isEvenInt()); // noexception
    List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    Collections2.removeIf(integers, NumberPredicates.isEvenInt());
    assertEquals(integers, Arrays.asList(1, 3, 5, 7, 9));
    Collections2.retainIf(integers, NumberPredicates.isOddInt());
    assertEquals(integers, Arrays.asList(1, 3, 5, 7, 9));
    Collections2.retainIf(integers, NumberPredicates.isEvenInt());
    assertEquals(Collections.<Integer>emptyList(), integers);
  }

  @Test
  public void testForeach() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = new ArrayList<String>();
      Collections2.foreach(integers, new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          list.add(arg.toString());
          return null;
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = new ArrayList<String>();
      Collections2.foreach(integers, new Function1<Void, Integer>() {
        @Override
        public Void apply(Integer arg) {
          list.add(arg.toString());
          return null;
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMap() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = new ArrayList<String>();
      Collections2.map(integers, list, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = new ArrayList<String>();
      Collections2.map(integers, list, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMapClss() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      List<String> list = Collections2.map(integers, new TypeRef<ArrayList<String>>(){}, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertTrue(list instanceof ArrayList);
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list =  Collections2.map(integers, new TypeRef<LinkedList<String>>(){}, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertTrue(list instanceof LinkedList);
      assertEquals(Collections.<String>emptyList(), list);
    }
  }


  @Test
  public void testMapToArray() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = Collections2.mapToArray(integers, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = Collections2.mapToArray(integers, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testMapToList() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      final List<String> list = Collections2.mapToList(integers, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), list);
    }
    {
      List<Integer> integers = Collections.emptyList();
      final List<String> list = Collections2.mapToList(integers, new Function1<String, Integer>() {
        @Override
        public String apply(Integer arg) {
          return arg.toString();
        }
      });
      assertEquals(Collections.<String>emptyList(), list);
    }
  }

  @Test
  public void testAccumulate() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5);
    assertEquals(Integer.valueOf(26), Collections2.accumulate(integers, 11, Numeric.getSumInt()));
    assertEquals(Integer.valueOf(240), Collections2.accumulate(integers, 2, Numeric.getMultInt()));
    assertEquals(Integer.valueOf(2), Collections2.accumulate(Collections.<Integer>emptyList(), 2, Numeric.getMultInt()));
  }

  @Test
  public void testFill() {
    List<Integer> list = new ArrayList<Integer>();
    Collections2.fill(list, 5, new Function0<Integer>() {
      int i = 0;
      @Override
      public Integer apply() {
        return i++;
      }
    });
    assertEquals(Arrays.asList(0, 1, 2, 3, 4), list);
  }

  @Test
  public void testFilter() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    List<Integer> evens = new ArrayList<Integer>();
    Collections2.foreach(Collections2.filter(integers, NumberPredicates.isEvenInt()), Collect.into(evens));
    assertEquals(Arrays.asList(2, 4, 6), evens);
    evens.clear();
    Collections2.foreach(Collections2.filter(Collections.<Integer>emptyList(), NumberPredicates.isEvenInt()), Collect.into(evens));
    assertEquals(Collections.<Integer>emptyList(), evens);
  }

  @Test
  public void testCount() {
    List<Integer> integers =  Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    assertEquals(7, Collections2.count(integers));
  }

  @Test
  public void testCollect() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = new HashSet<Integer>();
      Collections2.collect(integers, integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = new HashSet<Integer>();
      Collections2.collect(integers, integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToClss() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = Collections2.collect(integers, new TypeRef<HashSet<Integer>>(){});
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = Collections2.collect(integers, new TypeRef<HashSet<Integer>>(){});
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToSet() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      Set<Integer> integers2 = Collections2.collectToSet(integers);
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      Set<Integer> integers2 = Collections2.collectToSet(integers);
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToList() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      LinkedList<Integer> integers2 = Collections2.collectToList(integers);
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      LinkedList<Integer> integers2 = Collections2.collectToList(integers);
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testCollectToArray() {
    {
      List<Integer> integers = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      ArrayList<Integer> integers2 = Iterators.collectToArray(integers.iterator());
      assertEquals(integers.toArray(), integers2.toArray());
    }
    {
      List<Integer> integers = Collections.emptyList();
      ArrayList<Integer> integers2 = Collections2.collectToArray(integers);
      Iterators.collect(integers.iterator(), integers2);
      assertEquals(integers.toArray(), integers2.toArray());
    }
  }

  @Test
  public void testHashMap() {
    {
      HashMap<String, Integer> m = Collections2.hashMap(Tuples.t("test", 0), Tuples.t("test1", 1), Tuples.t("test2", 2));
      assertEquals(Integer.valueOf(0), m.get("test"));
      assertEquals(Integer.valueOf(1), m.get("test1"));
      assertEquals(Integer.valueOf(2), m.get("test2"));
      assertEquals(3, m.size());
    }
    {
      HashMap<String, Integer> m = Collections2.hashMap();
      assertEquals(Collections.<String, Integer>emptyMap(), m);
    }
  }

  @Test
  public void testTreeMap() {
    {
      TreeMap<String, Integer> m = Collections2.treeMap(Tuples.t("test", 0), Tuples.t("test1", 1), Tuples.t("test2", 2));
      assertEquals(Integer.valueOf(0), m.get("test"));
      assertEquals(Integer.valueOf(1), m.get("test1"));
      assertEquals(Integer.valueOf(2), m.get("test2"));
      assertEquals(3, m.size());
    }
    {
      TreeMap<String, Integer> m = Collections2.<String, Integer>treeMap();
      assertEquals(Collections.<String, Integer>emptyMap(), m);
    }
  }

  @Test
  public void testTreeMap2() {
    Comparator<Tuple1<String>> c = new Comparator<Tuple1<String>>() {
      @Override
      public int compare(Tuple1<String> o1, Tuple1<String> o2) {
        return Comparators.compare(o1.first(), o2.first(), Comparators.NullStrategy.NULL_LAST);
      }
    };
    {
      TreeMap<Tuple1<String>, Integer> m = Collections2.treeMap(c, Tuples.t(Tuples.t("test"), 0), Tuples.t(Tuples.t("test1"), 1), Tuples.t(Tuples.t("test2"), 2));
      assertEquals(Integer.valueOf(0), m.get(Tuples.t("test")));
      assertEquals(Integer.valueOf(1), m.get(Tuples.t("test1")));
      assertEquals(Integer.valueOf(2), m.get(Tuples.t("test2")));
      assertEquals(3, m.size());
    }
    {
      TreeMap<Tuple1<String>, Integer> m = Collections2.treeMap(c);
      assertEquals(Collections.<Tuple1<String>, Integer>emptyMap(), m);
    }
  }

}
