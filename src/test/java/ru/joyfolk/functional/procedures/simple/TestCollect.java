package ru.joyfolk.functional.procedures.simple;

import org.junit.Test;
import ru.joyfolk.functional.procedures.Procedure1;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

public class TestCollect {
  @Test
  public void test() {
    Collection<String> test = new ArrayList<String>();
    Procedure1<String> collect = Collect.into(test);
    collect.apply("123");
    assertTrue(test.contains("123"));
  }
}
