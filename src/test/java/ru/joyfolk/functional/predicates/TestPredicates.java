package ru.joyfolk.functional.predicates;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPredicates{
  Predicate<Void> pTrue = new True<Void>();
  Predicate<Void> pFalse = new False<Void>();

  @Test
  public void testNot() {
    Predicate<Void> notTrue = new Not<Void>(pTrue);
    Predicate<Void> notFalse = new Not<Void>(pFalse);
    assertFalse(notTrue.apply(null));
    assertTrue(notFalse.apply(null));
  }

  @Test
  public void testXor() {
    Predicate<Void> xorTrueTrue = new Xor<Void>(pTrue, pTrue);
    Predicate<Void> xorFalseFalse = new Xor<Void>(pFalse, pFalse);
    Predicate<Void> xorTrueFalse = new Xor<Void>(pTrue, pFalse);
    Predicate<Void> xorFalseTrue = new Xor<Void>(pFalse, pTrue);
    assertFalse(xorTrueTrue.apply(null));
    assertFalse(xorFalseFalse.apply(null));
    assertTrue(xorTrueFalse.apply(null));
    assertTrue(xorFalseTrue.apply(null));    
  }


  @Test
  public void testAnd() {
    Predicate<Void> andTrueTrue = new And<Void>(pTrue, pTrue);
    Predicate<Void> andFalseFalse = new And<Void>(pFalse, pFalse);
    Predicate<Void> andTrueFalse = new And<Void>(pTrue, pFalse);
    Predicate<Void> andFalseTrue = new And<Void>(pFalse, pTrue);
    Predicate<Void> andTrueTrueTrueTrue = new And<Void>(pTrue, pTrue, pTrue, pTrue);
    Predicate<Void> andTrueTrueFalseTrue = new And<Void>(pTrue, pTrue, pFalse, pTrue);
    assertTrue(andTrueTrue.apply(null));
    assertFalse(andFalseFalse.apply(null));
    assertFalse(andTrueFalse.apply(null));
    assertFalse(andFalseTrue.apply(null));
    assertTrue(andTrueTrueTrueTrue.apply(null));
    assertFalse(andTrueTrueFalseTrue.apply(null));        
  }

  @Test
  public void testOr() {
    Predicate<Void> orTrueTrue = new Or<Void>(pTrue, pTrue);
    Predicate<Void> orFalseFalse = new Or<Void>(pFalse, pFalse);
    Predicate<Void> orTrueFalse = new Or<Void>(pTrue, pFalse);
    Predicate<Void> orFalseTrue = new Or<Void>(pFalse, pTrue);
    Predicate<Void> orTrueTrueTrueTrue = new Or<Void>(pTrue, pTrue, pTrue, pTrue);
    Predicate<Void> orTrueTrueFalseTrue = new Or<Void>(pTrue, pTrue, pFalse, pTrue);
    Predicate<Void> orFalseFalseFalseFalse= new Or<Void>(pFalse, pFalse, pFalse, pFalse);
    assertTrue(orTrueTrue.apply(null));
    assertFalse(orFalseFalse.apply(null));
    assertTrue(orTrueFalse.apply(null));
    assertTrue(orFalseTrue.apply(null));
    assertTrue(orTrueTrueTrueTrue.apply(null));
    assertTrue(orTrueTrueFalseTrue.apply(null));
    assertFalse(orFalseFalseFalseFalse.apply(null));        
  }

}
