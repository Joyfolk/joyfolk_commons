package ru.joyfolk.functional.predicates.simple;

import org.junit.Test;
import ru.joyfolk.functional.predicates.Predicate;

import static org.junit.Assert.*;

public class TestSimple {
  @Test
  public void test() {
    {
      Predicate<String> isEquals = new IsEquals<String>("test");
      assertTrue(isEquals.apply("test"));
      assertFalse(isEquals.apply("Test"));
    }
    {
      Predicate<Integer> isEven = NumberPredicates.isEvenInt();
      assertTrue(isEven.apply(4));
      assertFalse(isEven.apply(5));
      assertTrue(isEven.apply(0));
      Predicate<Integer> isOdd = NumberPredicates.isOddInt();
      assertFalse(isOdd.apply(4));
      assertTrue(isOdd.apply(5));
      assertFalse(isOdd.apply(0));
    }
    {
      Predicate<Long> isEven = NumberPredicates.isEvenLong();
      assertTrue(isEven.apply((long) 4));
      assertFalse(isEven.apply((long) 5));
      assertTrue(isEven.apply((long) 0));
      Predicate<Long> isOdd = NumberPredicates.isOddLong();
      assertFalse(isOdd.apply((long) 4));
      assertTrue(isOdd.apply((long) 5));
      assertFalse(isOdd.apply((long) 0));
    }
    {
      Predicate<Integer> isLarger = IsLarger.than(5);
      assertFalse(isLarger.apply(3));
      assertFalse(isLarger.apply(5));
      assertTrue(isLarger.apply(7));
      Predicate<Integer> isLargerOrEquals = IsLargerOrEquals.than(5);
      assertFalse(isLargerOrEquals.apply(3));
      assertTrue(isLargerOrEquals.apply(5));
      assertTrue(isLargerOrEquals.apply(7));
    }
    {
      Predicate<Integer> isLesser = IsLesser.than(5);
      assertTrue(isLesser.apply(3));
      assertFalse(isLesser.apply(5));
      assertFalse(isLesser.apply(7));
      Predicate<Integer> isLesserOrEquals = IsLesserOrEquals.than(5);
      assertTrue(isLesserOrEquals.apply(3));
      assertTrue(isLesserOrEquals.apply(5));
      assertFalse(isLesserOrEquals.apply(7));
    }

  }
}
