package ru.joyfolk.concurent.actors;

public class TestActorFactory {
  // todo: ! not working
//	ExecutorService executorService;
//
//	@Before
//	public void startExecutorService() {
//		executorService = Executors.newFixedThreadPool(4);
//	}
//
//	@After
//	public void stopExecutorService() {
//		executorService.shutdown();
//	}
//
//	@Test
//	public void testActorFactoryAndCancel() throws InterruptedException {
//		ActorFactory af = new ActorFactory(executorService);
//		af.cancel(true);
//		Thread.sleep(1000);
//		assertTrue(af.isCanceled());
//	}
//
//	public static interface LogActor {
//		public void receiveMessage(String msg);
//
//		@Synchronous
//		public String getLog();
//	}
//
//	public static interface TimeActor {
//		public void sendMessages(LogActor logActor);
//
//		@Synchronous
//		public boolean completed();
//	}
//
//
//	public static class LogActorImpl implements LogActor {
//		private final List<String> log = new ArrayList<String>();
//		private final Random rand = new Random(12345);
//
//		public void receiveMessage(String msg)  {
//			log.add(msg);
//			try {
//	      Thread.sleep(rand.nextInt(100));
//      } catch (InterruptedException e) { }
//		}
//
//		@Override
//		public String getLog() {
//			return Strings.join("\n", log);
//		}
//	}
//
//	public static class TimeActorImpl implements TimeActor {
//		private final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//
//		@Override
//		public void sendMessages(LogActor actor) {
//			for(int i = 0; i < 10; i++)
//				actor.receiveMessage(fmt.format(new Date()));
//			complete = true;
//		}
//
//		private volatile boolean complete = false;
//
//		@Override
//    public boolean completed() {
//	    return complete;
//    }
//	}
//
//	@Test
//	public void testMakeActor() throws InterruptedException {
//		ActorFactory af = new ActorFactory(executorService);
//		LogActor logActor = af.makeActor(new LogActorImpl(), LogActor.class);
//		TimeActor timeActor = af.makeActor(new TimeActorImpl(), TimeActor.class);
//		timeActor.sendMessages(logActor);
//		while(!timeActor.completed()) ;
//		System.out.print(logActor.getLog());
//	}

}
