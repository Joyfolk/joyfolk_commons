package ru.joyfolk.strings;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestStrings {
  @Test
  public void testIsEmpty() {
    assertTrue(Strings.isEmpty(null));
    assertTrue(Strings.isEmpty(""));
    assertTrue(Strings.isEmpty("   "));
    assertFalse(Strings.isEmpty("fff"));
    assertFalse(Strings.isEmpty(" fff "));
  }
}
